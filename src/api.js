import axios from "axios";
import Cookies from 'universal-cookie';

import {
	encrypt,
	decrypt
} from "./encryption";

import {
	api,
	baseurl
} from "./env";

const cookies = new Cookies();

class Api{
	static async fetch(requests){
		const fetch = requests.map(request=>{
			switch(request.method){
				case "post":
				case "POST":
					return Api.post(request)
				default:
					return Api.get(request)
			}
		});

		let responses = await Promise.all(fetch);

		return responses.filter(res=>res.status===200).map(response=>JSON.parse(decrypt(response.data)));
	}

	static get(request){
		return axios.get(`${baseurl}${api}${request.url}`, {
			headers: {
				mac_address: localStorage.getItem('macAddress'),
				auth_token: cookies.get("auth_token"),
				language: cookies.get("language")
			},
			params: request.body ? {
				ra: encrypt(JSON.stringify(request.body))
			} : null 
	})
		.catch(async error=>error)
	}

	static post(request){
		if (request.url === "booking") 
			cookies.set("auth_token", encrypt(JSON.stringify(request.body)).substring(0, 50))

		return axios.post(`${baseurl}${api}${request.url}`, {
			ra: request.body ? encrypt(JSON.stringify(request.body)) : null
		}, {
			headers: {
				mac_address: localStorage.getItem('macAddress'),
				auth_token: cookies.get("auth_token"),
				language: cookies.get("language")
			}
		})
		.catch(async error=>await error)
	}
}

export default Api;