import React, { Component } from 'react';

import "./style.css";

class Layout extends Component {

  handleKeyDown(e){
    if (this.props.numberOnly === true && isNaN(e.key) && e.key !== "Backspace" && e.key !== "Enter") {
      e.preventDefault();
      return;
    }

    if (e.keyCode !== 13) return;
    
    this.props.handleEnterPress(e);
  }

  render(){
  	const {
  		className,
      disabled,
      name,
      placeholder,
      readOnly,
      type,
      value,

      handleChange
  	} = this.props
    
    return(
	    <input type={type} name={name} className={className} placeholder={placeholder} disabled={disabled} onChange={handleChange} onKeyDown={this.handleKeyDown.bind(this)} readOnly={readOnly === true} value={value}/>
    );
	}
}

export default Layout;