import React, { Component } from 'react';

import Button from "../Button"

import "./style.css";

class Layout extends Component {
  handleBackClick(){
    if (this.props.handleBackClick) {
      this.props.handleBackClick()
      return;
    }

    this.props.history.goBack();
  }

  handleRightButtonClick(e){
    this.props.handleContinueClick(e)
  }

  render(){
		const {
      rightText,
      renderRight
		} = this.props

    const renderRightFunc = ()=>{
      if (renderRight === false) return null;

      return (
        <Button className={"right"} handleClick={this.handleRightButtonClick.bind(this)}>{rightText ? rightText : "Next"}</Button>
      );
    }

    return(
    	<div className={"pal-footer"}>
    		<Button className={"left"} handleClick={this.handleBackClick.bind(this)}>Back</Button>

    		{renderRightFunc()}
    	</div>
    );
	}
}

export default Layout;