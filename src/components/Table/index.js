import React, { Component } from 'react';

import "./style.css";

class Layout extends Component {
  	render(){
  		const {
  			head,
        body
  		} = this.props

	    return(
	    	<table>
          <thead>
            {head}
          </thead>
          <tbody>
            {body}
          </tbody>
        </table>
	    );
	}
}

export default Layout;