import React, { Component } from 'react';

import "./style.css";

class Layout extends Component {
  	render(){
  		const {
  			checked,
  			children,

  			handleChange
  		} = this.props

	    return(
	    	<label className={"pal-checkbox"}>
				<div>
					<input type={"checkbox"} checked={checked} onChange={handleChange}/>
					<span className={"checkmark"}></span>
				</div>

				<div>
					{children}
				</div>
			</label>
	    );
	}
}

export default Layout;