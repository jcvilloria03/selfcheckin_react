import React, { Component } from 'react';

import Ink from "react-ink"

import "./style.css";
import close from "../../assets/img/signature-clear.png"

class Layout extends Component {
  	handleCancelClick(){
  		setTimeout(()=>{
  			window.location.href = "/"
  		}, 300)
  	}

  	render(){
	    return(
	    	<button className={"pal-cancel"} onClick={this.handleCancelClick.bind(this)}>
	    		<img alt={"Cancel"} src={close} />
	    		<Ink />
	    	</button>
	    );
	}
}

export default Layout;