import React, { Component } from 'react';
import moment from "moment-timezone"
import DatePicker from "react-datepicker";

import {
  timezone
} from "../../env"
 
import "react-datepicker/dist/react-datepicker.css";
import "./style.css";

class Layout extends Component {
  render(){    
    const {
      minDate,
      maxDate,
      placeholder,
      selected,
      className,
      format,

      handleChange
    } = this.props

    const strDate = selected ? selected.getFullYear() + "-" + (selected.getMonth() + 1) + "-" + selected.getDate() : null
    return(
	    <DatePicker
        customInput={<button>{selected ? moment.tz(strDate, "YYYY-MM-DD", timezone).format(format ? format : "MMMM DD, YYYY") : placeholder}</button>}
        selected={selected}
        onChange={handleChange}
        minDate={minDate}
        maxDate={maxDate}
        showYearDropdown
        showMonthDropdown
        className={className} />
    );
	}
}

export default Layout;