import React, { Component } from 'react'
import Dropdown from 'react-dropdown'

import 'react-dropdown/style.css'
import "./style.css"

class Layout extends Component {
	render(){
		const {
			className,
			placeholder,
			options,
			value,

      		handleChange
		} = this.props

    	return(
    		<Dropdown className={className} options={options} onChange={handleChange} placeholder={placeholder} value={value}/>
    	);
	}
}

export default Layout;