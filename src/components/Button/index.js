import React, { Component } from 'react';

import Ink from "react-ink"

import "./style.css";

class Layout extends Component {
	state = {
		clicked: false
	}

	handleClick(){
		if (this.state.clicked) return;

		this.setState({clicked: true});

		this.setState({clicked: false});
		this.props.handleClick();
	}

  	render(){
  		const {
  			children,
  			className,
  		} = this.props

  		const {
  			clicked
  		} = this.state

	    return(
	    	<button className={`pal-button ${className ? className : ""}${clicked ? " clicked" : ""}`} onClick={this.handleClick.bind(this)}>
	    		<Ink />
	    		{children}
	    	</button>
	    );
	}
}

export default Layout;