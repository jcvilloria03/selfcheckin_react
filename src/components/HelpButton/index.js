import React, { Component } from 'react';
import io from "socket.io-client"

import {
	websocket
} from "../../env"

import "./style.css";
import help from "../../assets/img/help.png"

class Layout extends Component {
	socket = io(`${websocket}/`, {
		transports: ['websocket'], 
		upgrade: false,
		query: `macAddress=${localStorage.getItem('macAddress')}`
	});

  	handleCancelClick(){
  		setTimeout(()=>{
  			this.socket.emit("help")
  		}, 300)
  	}

  	render(){
	    return(
	    	<button className={"pal-help"} onClick={this.handleCancelClick.bind(this)}>
	    		<img alt={"Cancel"} src={help} />
	    		HELP
	    	</button>
	    );
	}
}

export default Layout;