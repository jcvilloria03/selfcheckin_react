import React, { Component } from 'react';

import Ink from "react-ink"

import "./style.css";

class Layout extends Component {
	state = {
		clicked: false
	}

	handleClickFunction(e){
		if (this.state.clicked) return;

		this.setState({
			clicked: true
		});

		setTimeout(()=>{
			this.setState({ clicked: false });
			this.props.handleClick();
		}, 200);
	}

  	render(){
  		const {
  			className,
  			icon
  		} = this.props

  		const {
  			clicked
  		} = this.state

	    return(
	    	<button className={`pal-button-image ${className}${clicked ? " clicked" : ""}`} onClick={this.handleClickFunction.bind(this)}>
	    		<img alt={"Icon"} src={icon}/>
	    		<Ink />
	    	</button>
	    );
	}
}

export default Layout;