import { applyMiddleware, createStore, combineReducers } from "redux";

import app from "./app/reducer";
import breakfast from "./modelview/breakfast/reducer";
import cardDetails from "./modelview/card-details/reducer";
import cardNew from "./modelview/card-new/reducer";
import checkinConfirmation from "./modelview/checkin-confirmation/reducer";
import checkStatus from "./modelview/checkstatus/reducer";
import checkoutConfirmation from "./modelview/checkout-confirmation/reducer";
import checkoutBill from "./modelview/checkout-bill/reducer";
import companionDetails from "./modelview/companion-details/reducer";
import companionSignature from "./modelview/companion-signature/reducer";
import earlyCheckin from "./modelview/early-checkin/reducer";
import guest from "./modelview/guest/reducer";
import lateCheckout from "./modelview/late-checkout/reducer";
import passportScan from "./modelview/passport-scan/reducer";
import preCheckin from "./modelview/pre-checkin/reducer";
import preCheckout from "./modelview/pre-checkout/reducer";
import reservationDetails from "./modelview/reservation-details/reducer";
import roomChoices from "./modelview/room-choices/reducer";
import roomDescription from "./modelview/room-description/reducer";
import roomPreferences from "./modelview/room-preferences/reducer";
import welcome from "./modelview/welcome/reducer";
import makeKey from "./modelview/make-key/reducer";

import { Logger } from "./middleware";

const reducer = combineReducers({
	app,
	breakfast,
	cardDetails,
	cardNew,
	checkinConfirmation,
	checkStatus,
	checkoutConfirmation,
	checkoutBill,
	companionDetails,
	companionSignature,
	earlyCheckin,
	guest,
	lateCheckout,
	passportScan,
	preCheckin,
	preCheckout,
	reservationDetails,
	roomChoices,
	roomDescription,
	roomPreferences,
	welcome,
	makeKey
});

const middleware = applyMiddleware(Logger);
const store = createStore(reducer, middleware);

export default store;