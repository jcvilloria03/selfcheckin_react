import React, { Component } from 'react';

import {
  Button
} from "../../components"

class Layout extends Component {
  render(){
    const {
      image,
      text,

      handleMoreScan
    } = this.props

    return(
      <div className={"animate-wrapper"}>
  	    <div className={"details-container align-center"}>
          <h1 className={"font-normal color-primary align-center"}>{text.title}</h1>
          <div className={"image-wrapper"}>
            {image ? (<img alt={"Guest"} src={`data:image/jpeg;base64,  ${image}`}/>) : null}
          </div>
          <Button handleClick={handleMoreScan}>{text.buttonMore}</Button>
        </div>
      </div>
    );
	}
}

export default Layout;