import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			scan: {
				buttonMore: data.button_more,
				title: data.title,
				button: data.button,
				header: data.header,
				instruction: data.instruction
			},
			error: {
				technical: data.error_technical,
				scan: data.error_scan
			}
		}
	}
}

export default Action;