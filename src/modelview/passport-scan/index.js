import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import io from "socket.io-client"
import iso from 'iso-3166-1';
import Cookies from 'universal-cookie';

import {
	Footer
} from "../../components"

import {
	websocket,
} from "../../env"

import Action from "./action"
import Api from "../../api"
import Details from "./details"
import GuestAction from "../guest/action";
import Loader from 'react-loader-spinner'
import Scan from "./passport"

import "./style.css";

const cookies = new Cookies();

class View extends Component {
	page = "pal-passport-scan";

	state = {
		companionScan: 0,
		detailsView: false,
		loader: false,
		scannerID: null,
		scanButton: false,
		image: null,
		error: {
			status: false,
			message: null
		},
	}

	socket = null
	timer = null

	componentWillMount(){
		this.props.dispatch(GuestAction.unsetGuestInfoFull());
		cookies.remove("auth_token")
	}

	async componentDidMount(){
		let requests = [
			{
				url: "interface",
				body: {
					name: "passport_scan"
				}
			},
			{
				url: "passportscannerid"
			}
		];

		const [text, passport] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))

		if (!passport){
			this.handleError(true, this.props.text.error.technical);
			return;
		}
		

		this.setState({
			scannerID: passport.scanner_id_card_number
		});

		this.socket = io(`${websocket}/`, {
			transports: ['websocket'], 
			upgrade: false,
			query: `macAddress=${localStorage.getItem('macAddress')}`
		});

		this.socket.on("finish", this.onScan.bind(this))
		this.socket.on("scanning", this.onScanning.bind(this))
	}

	async setGuestDetails(details){		
		if (details.status === false) {
			return;
		}

		details = {
			passportNumber: details.passportNumber ? details.passportNumber : null,
			passportExpiration: details.passportExpiration ? details.passportExpiration : null,
			passportIssuing: details.issuingcountry ? iso.whereAlpha3(details.issuingcountry).alpha2 : null,
			passportImage: details.image,
			passportPhoto: details.photo,
			firstname: details.firstname ? details.firstname : null,
			lastname: details.lastname ? details.lastname : null,
			birthdate: details.birthday ? details.birthday : null,
			country: details.country ? iso.whereAlpha3(details.country).alpha2 : null,
			gender: details.gender ? (details.gender === "M" ? "MALE" : "FEMALE") : null,
		}

		if (this.state.companionScan === 0 && details.passportNumber) {
			this.props.dispatch(GuestAction.setGuestInfoViaPassportScan(details))
		}else if(details.passportNumber){
			this.props.dispatch(GuestAction.addCompanionViaScan(details))
		}

		this.setState({
			companionScan: (this.state.companionScan + 1),
			detailsView: true,
			loader: false,
			image: details.passportImage,
		});
	}

	async handleContinueClick(){
		if (!this.props.passportNumber) {
			this.handleError(true, this.props.text.error.scan)
			return;
		}

		this.setState({detailsView: false})

		this.props.history.push("/pre-checkin")
	}

	handleScanClick(){
		this.setState({
			loader: true,
		});

		this.socket.emit("scan", {
			idCard: this.state.scannerID
		});

		this.timer = setTimeout(()=>{
			this.handleError(true, this.props.text.error.technical);
		}, 1000 * 20)
	}

	handleError(error, message){
		this.setState({
			loader: false,
			error: {
				...this.state.error,
				status: error,
				message
			}
		})
	}

	onScan(data){
		clearTimeout(this.timer)

		if (data.status === "true") {
			this.setGuestDetails(data);
			this.handleError(false, "")
			return;
		}

		this.setState({
			loader: false
		});

		if (data.status !== "Error 5") {
			this.handleError(true, this.props.text.error.technical)
			return;
		}

		this.handleError(true, this.props.text.error.scan)
	}

	onScanning(){
		clearTimeout(this.timer)

		this.timer = setTimeout(()=>{
			this.handleError(true, this.props.text.error.technical);
		}, 1000 * 20)
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			detailsView,
  			loader,
  			error,
  			image
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Passport Scan</title>
	    		</Header>
	    		<br/> 
	    		<br/>

	    		<p className={`pal-error${error.status ? " active" : ""}`} onClick={this.handleError.bind(this, false, null)}>
		          { error.message }
		        </p>
	    		{detailsView ? (<Details text={text.scan} image={image} handleMoreScan={this.handleScanClick.bind(this)}/>) : (<Scan text={text.scan} handleScanClick={this.handleScanClick.bind(this)}/>)}

	    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		text: store.passportScan.text
	}
})(View);