import {
	TEXT_SET
} from "./constant";

const initialState = {
	text: {
		scan: {}
	}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return {
		...state,
		text: {
			scan: action.scan,
			error: action.error
		}
	}
}

export default reducer;