import React, { Component } from 'react';

import {
  Button
} from "../../components"

class Layout extends Component {
  render(){
    const {
      text,

      handleScanClick
    } = this.props

    return(
      <div className={"animate-wrapper"}>
        <h1 className={"font-normal color-primary"}>{text.title}</h1>

        <h2 className={"font-normal"}>{text.header}</h2>

        <br/>
        <br/>

        <p className={"font-italic color-gray"}>{text.instruction}</p>

        <br/>
        <br/>

        <Button handleClick={()=>handleScanClick()}>{text.button}</Button>
      </div>
    );
	}
}

export default Layout;