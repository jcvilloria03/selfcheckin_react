import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import "./style.css";

class View extends Component {
	page = "pal-checkout-success";

	state = {
		timer: 6
	}

	componentDidMount(){
		this.timer = setInterval(this.handleTick.bind(this), 1000);
	}

	componentWillUnmount(){
		clearInterval(this.timer);
	}

	handleTick(){
		console.log(this.state.timer-1);
		if (this.state.timer < 1)
			window.location.href = "/";

		this.setState({timer: this.state.timer === 0 ? 0 : this.state.timer-1});
	};

  	render(){  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Checkout Success</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>SUCCESS</h1>
	    			<h2 className={"font-normal"}>Thank you, you are now checked-out. I hope you had a pleasant stay</h2>

	    			<p className={"color-gray font-italic"}>Reloading in {this.state.timer} {this.state.timer < 2 ? "second" : "seconds"}</p>
	    		</div>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {

	}
})(View);