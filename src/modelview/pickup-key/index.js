import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import Api from "../../api"
import GuestAction from "../guest/action"

import {
	Footer,
	Textbox
} from "../../components"

import "./style.css";

class View extends Component {
	state = {
		password: "",
		error: {
			status : false,
			message: ""
		}
	}

	page = "pal-pickup-key"

	componentWillMount(){
		this.props.dispatch(GuestAction.unsetGuestInfoFull());
	}

	handlePasswordChange = (e)=>{
		this.setState({
			password: e.currentTarget.value
		})
	}

	handleContinueClick = async ()=>{
		if (!this.state.password) {
			this.manageError(true, "Password field is empty.")()
			return;
		}

		let requests = [
			{url: "pickupkey/auth", method: "post", body: {
				passcode: this.state.password
			}}
		];

		const [response] = await Api.fetch(requests);

		if (!response.status) {
			this.manageError(true, "No booking found.")()
			return
		}

		await this.props.dispatch(GuestAction.setGuestInfoViaPickup(response.data))
		this.props.history.push("/make-key")
	}

	manageError = (status, message) => (e) => {
		this.setState({
			error: {
				status,
				message
			}
		})
	}

  	render(){
  		const {
  			history
  		} = this.props
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page} title={"Pickup Key"}/>

	    		<p className={`pal-error${this.state.error.status ? " active" : ""}`} onClick={this.manageError(false, "")}>
    				{this.state.error.message}
    			</p>
    			<br/><br/>

	    		<div className={"animate-wrapper"}>
	    			<h1>PICK UP KEY</h1>
	    			<p className={"header"}>Please enter the Password to continue</p>

	    			<Textbox type="text" className={"align-center"} value={this.state.password} placeholder={"Password"} handleChange={this.handlePasswordChange} handleEnterPress={this.handleContinueClick}/>
	    		</div>

	    		<Footer history={history} handleContinueClick={this.handleContinueClick}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {

	}
})(View);