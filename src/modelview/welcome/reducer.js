import {
	LANGUAGE_SET,
	TEXT_SET
} from "./constant";

const initialState = {
	language: [],
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case LANGUAGE_SET:
			return setLanguage(state, action);
		case TEXT_SET:
			return setText(state, action);
		default:
			return state;
	}
}

function setLanguage(state, action){
	return {
		...state,
		language: action.language
	}
}

function setText(state, action){
	return {
		...state,
		text: action.text
	}
}

export default reducer;