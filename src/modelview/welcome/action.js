import {
	LANGUAGE_SET,
	TEXT_SET
} from "./constant";

class Action{
	static setLanguage(data){
		return {
			type: LANGUAGE_SET,
			language: data
		}
	}

	static setText(data){

		return {
			type: TEXT_SET,
			text: {
				language: {
					title: data.title
				},
				information: {
					title: data.information_title,
					header: data.information_sub,
					home: data.information_home,
					continue: data.information_continue,
				}
			}
		}
	}
}

export default Action;