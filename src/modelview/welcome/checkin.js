import React, { Component } from 'react';

import {
  Button
} from "../../components"

class Layout extends Component {
  handleSelectionClick(state){
    this.props.handleSelectionClick(state)
  }

  render(){    
    return(
	    <div className={"selection-container"}>
        <h1>DIGITAL CHECK-IN</h1>
        <Button className={"selection font-bold"} handleClick={this.handleSelectionClick.bind(this, 0)}>STAFF ASSISTED</Button>
        <Button className={"selection font-bold"} handleClick={this.handleSelectionClick.bind(this, 1)}>SELF CHECK-IN</Button>
      </div>
    );
	}
}

export default Layout;