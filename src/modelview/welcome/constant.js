const LANGUAGE_SET = "welcome/LANGUAGE_SET";
const TEXT_SET = "welcome/TEXT_SET";

export {
	LANGUAGE_SET,
	TEXT_SET
}