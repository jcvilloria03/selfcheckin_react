import React, { Component } from 'react';
import { connect } from "react-redux";
import Cookies from 'universal-cookie';

import Action from "./action"
import Api from "../../api"
import Checkin from "./checkin"
import GuestAction from "../guest/action"
import Information from "./information"
import Language from "./language"

import "./style.css";

const cookies = new Cookies();

class View extends Component {
	page = "pal-welcome";
	state = {
		isSelf: false,
		clickCount: 0,
		clicked: false
	}

	componentWillMount(){
		const language = cookies.get("language");

		this.props.dispatch(GuestAction.unsetGuestInfoFull());
		cookies.remove("auth_token")

		if (language)
			window.location.reload();
	}

	async componentDidMount(){
		let requests = [
			{url: "language"},
			{url: "interface", body: {
				name: "welcome"
			}}
		];

		const [language, welcome] = await Api.fetch(requests);

		this.props.dispatch(Action.setLanguage(language))
		this.props.dispatch(Action.setText(welcome))
	}

	handleLanguageClick(language){
		if (this.state.clicked) return;

		this.setState({clicked: true});
		cookies.set("language", language);

		setTimeout(()=>{
			this.props.history.push("check-status")
			this.props.handleRenderApp();
		}, 300);
	}

	handleSelectionClick(selected){
		if(selected > 0){
		 	this.setState({isSelf: selected});
		 	return
		}

		window.location.href = 'https://radius.palvision.com:8016/';
	}

	handleLogoClick(){
		this.setState({clickCount: (this.state.clickCount+1)});
		if(this.state.clickCount){
			window.location.href = "/";
		}
	}

	handleHomeClick(){
		this.setState({
			isSelf: 2
		})
	}

	handleContinueClick(){
		window.location.href = "/passport-scan";
	}

  	render(){
  		const {
  			language,
  			logo,
  			text,
  			video
  		} = this.props;

	    return(
	    	<div className={this.page}>
	    		<video autoPlay muted loop src={video}>
					Your browser does not support the video tag.
				</video>
				<img className={"pal-logo"} alt={"Logo"} src={logo} onClick={this.handleLogoClick.bind(this)}/>

				{
					this.state.isSelf === false ? (
						<Checkin handleSelectionClick={this.handleSelectionClick.bind(this)}/>
					) : (this.state.isSelf === 1 ? (
						<Information text={text.information} handleHomeClick={this.handleHomeClick.bind(this)} handleContinueClick={this.handleContinueClick.bind(this)}/>
					) : (
						<Language text={text.language} language={language} handleLanguageClick={this.handleLanguageClick.bind(this)}/>
					))	
				}
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		language: store.welcome.language,
		logo: store.app.logo,
		text: store.welcome.text,
		video: store.app.background.video
	}
})(View);