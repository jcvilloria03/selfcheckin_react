import React, { Component } from 'react';
import Ink from "react-ink"

class Layout extends Component {

  handleLanguageClick(code){
    this.props.handleLanguageClick(code)
  }

  render(){
    const {
      language,
      text,
    } = this.props;


    return(
	    <div className={"language-container"}>
        <div className={"color-white align-center note"} dangerouslySetInnerHTML={{__html: text.title}}/>

        <div className={"align-center flag-container"}>
          {language.map(data=>(
            <div key={data.language} className={"flag"} tabIndex="0" onClick={this.handleLanguageClick.bind(this, data.code)}>
              <Ink />
              <img alt={data.language} src={data.img}/>
              <div className={"color-white font-bold"}>{data.language}</div>
            </div>
          ))}
        </div>
      </div>
    );
	}
}

export default Layout;