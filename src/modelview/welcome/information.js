import React, { Component } from 'react';

import {
  Button
} from "../../components"

class Layout extends Component {
  timer = null;

  componentDidMount(){
    this.timer = setTimeout(()=>{
      this.props.handleHomeClick()
    }, 1000 * 60)
  }

  componentWillUnmount(){
    clearTimeout(this.timer)
  }

  handleHomeClick(){
    this.props.handleHomeClick()
  }

  handleContinueClick(){
    this.props.handleContinueClick()
  }

  render(){
    const {
      text,
    } = this.props;    

    return(
	    <div className={"information-container align-center"}>
        <h1 className={"color-white"}>{text.title}</h1>
        <br/>
        <br/>
        <br/>
        <h2 className={"color-white"}>{text.header}</h2>
        <br/>
        <br/>
        <br/>
        <Button className={"selection font-bold"} handleClick={this.handleHomeClick.bind(this)}>{text.home}</Button>
        <Button className={"selection font-bold"} handleClick={this.handleContinueClick.bind(this)}>{text.continue}</Button>
      </div>
    );
	}
}

export default Layout;