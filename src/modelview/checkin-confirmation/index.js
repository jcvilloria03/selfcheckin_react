import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"
import Loader from 'react-loader-spinner'

import {
	Button,
	Checkbox,
	Footer
} from "../../components"

import {
	timezone
} from "../../env"

import Action from "./action"
import Api from "../../api"
import SignaturePad from 'react-signature-canvas'

import "./style.css";
import signatureClear from "../../assets/img/signature-clear.png"

class View extends Component {
	page = "pal-checkin-confirmation";

	signature = React.createRef();

	state = {
		terms: false,
		loader: false,
		error: false,
		errorMessage: "",
		signature: null,
		termsUI: {
			status: false,
			body: null
		}
	}

	async componentDidMount(){
		if (!this.props.guest.guestDetails.passport.number) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "checkin_confirmation"
			}},
			{url: "termsandconditions"}
		];

		const [text, terms] = await Api.fetch(requests);

		this.setState({termsUI: {status: false, body: terms.terms_and_conditions}})
		this.props.dispatch(Action.setText(text))
	}

	clearSignaturePad(){
		this.signature.current.clear();
	}
	
	async handleContinueClick(){
		if (this.signature.current.isEmpty()) {
			this.manageError(true, this.props.text.errorSignature)
			return;
		}

		if (!this.state.terms) {
			this.manageError(true, this.props.text.errorTerms)
			return;
		}

		this.setState({
			loader: true
		});

		const requestBody = {
			companion_list: this.props.guest.companionList,
			confirmation_number: this.props.guest.confirmationNumber,
			reservation_id: this.props.guest.reservationId,
			guest_details: {
				birthdate: this.props.guest.guestDetails.birthdate,
				email: this.props.guest.guestDetails.email,
				firstname: this.props.guest.guestDetails.firstname,
				gender: this.props.guest.guestDetails.gender,
				lastname: this.props.guest.guestDetails.lastname,
				mobile: this.props.guest.guestDetails.mobileNumber,
				profile_id: this.props.guest.guestDetails.profileId,
				address: {
					addressId: this.props.guest.guestDetails.address.id,
					city: this.props.guest.guestDetails.address.city,
					country: this.props.guest.guestDetails.address.country,
					postal: this.props.guest.guestDetails.address.postal,
					state: this.props.guest.guestDetails.address.state,
					street: this.props.guest.guestDetails.address.street
				},
				passport: {
					expiration: this.props.guest.guestDetails.passport.expiration,
					issuing_authority: this.props.guest.guestDetails.passport.issuingAuthority,
					number: this.props.guest.guestDetails.passport.number
				},
				room_details: {
					extra_preferences: this.props.guest.guestDetails.roomDetails.extraPreferences,
					preferences: this.props.guest.guestDetails.roomDetails.preferences,
					room_number: this.props.guest.guestDetails.roomDetails.roomNumber,

				},
				stay_details: {
					breakfast: this.props.guest.guestDetails.stayDetails.breakfast,
					early_checkin: this.props.guest.guestDetails.stayDetails.earlyCheckin,
					late_checkout: this.props.guest.guestDetails.stayDetails.lateCheckout
				}
			}
		}
		
		let requests = [
			{url: "processcheckin", method: "post", body: requestBody}
		];

		await Api.fetch(requests);

		this.props.history.push("/make-key")
	}

	async handleTermsCloseClick(){
		await this.setState({
			termsUI: {
				...this.state.termsUI,
				status: false
			}
		})

		this.signature.current.fromData(this.state.signature);
	}

	handleTermsClicked(){
		this.setState({
			signature: this.signature.current.toData(),
			terms: !this.state.terms,
			termsUI: {
				...this.state.termsUI,
				status: !this.state.terms
			}
		})
	}

	manageError(error, errorMessage){
		this.setState({
			error,
			errorMessage: errorMessage,
			loader: false
		});
	}

  	render(){
  		const {
  			guest,
  			history,
  			text
  		} = this.props

  		const {
  			error,
  			errorMessage,
  			loader,
  			termsUI,
  			terms
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);

  		if (termsUI.status)
  		return(
  			<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Checkin Confirmation</title>
	    		</Header>

	    		<div className={"animate-wrapper align-left terms-wrapper"} dangerouslySetInnerHTML={{__html: termsUI.body}}/>

	    		<Button className={"terms"} handleClick={this.handleTermsCloseClick.bind(this)}>OK</Button>
	    	</div>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Checkin Confirmation</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<p className={`pal-error${error ? " active" : ""}`} onClick={this.manageError.bind(this, false, null, null)}>
	    				{ errorMessage }
	    			</p>
	    			<br/>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<div className={"signature-wrapper"}>
    					<SignaturePad 
	    					ref={this.signature}
                            penColor={"#000"}
				            backgroundColor={'#fff'}
				            marginBottom={'20px'}
				            canvasProps={{width: 500, height: 250}}
                        />
                        <button onClick={this.clearSignaturePad.bind(this)}><img alt={"Clear"} src={signatureClear}/></button>
                        <p className={"font-italic color-gray align-center email-instuction"}>{text.signatureInstruction}</p>
    				</div>

	    			<div className={"details align-left"}>
	    				<h2 className={"align-left font-normal"}>{text.stay_details_label}</h2>
		    			<p>{text.checkinLabel}: <b>{moment.tz(guest.guestDetails.stayDetails.arrivalDate, timezone).format("MMMM DD, YYYY")}</b></p>
		    			<p>{text.checkoutLabel}: <b>{moment.tz(guest.guestDetails.stayDetails.departureDate, timezone).format("MMMM DD, YYYY")}</b></p>
		    			<p>{text.nightsLabel}: <b>{guest.guestDetails.stayDetails.noOfNights}</b></p>
		    			<p>{text.guestLabel}: <b>{guest.guestDetails.stayDetails.adultCount} Adult{guest.guestDetails.stayDetails.adultCount > 1 ? "s" : ""}, {guest.guestDetails.stayDetails.childCount} Child{guest.guestDetails.stayDetails.childCount > 1 ? "s" : ""}</b></p>
		    			<p>{text.packageLabel}: <b>{guest.guestDetails.roomDetails.roomPackage.toString()}</b></p>
		    			<p>{text.roomTypeLabel}: <b>{guest.guestDetails.roomDetails.roomType}</b></p>
		    			<p>{text.preferencesLabel}: <b>{Object.keys(guest.guestDetails.roomDetails.preferences).map(code=>(`${guest.guestDetails.roomDetails.preferences[code].name}`)).toString().replace(/,/g, ", ")}</b></p>
		    			<p>{text.extraPreferencesLabel}: <b>{Object.keys(guest.guestDetails.roomDetails.extraPreferences).map(code=>(`${guest.guestDetails.roomDetails.extraPreferences[code].name}`)).toString().replace(/,/g, ", ")}</b></p>
		    			<br/>
		    			<Checkbox checked={terms} handleChange={this.handleTermsClicked.bind(this)}>{text.termsConditionLabel}</Checkbox>
		    			<Checkbox>{text.promotionalLabel}</Checkbox>
		    		</div>
	    		</div>

	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		text: store.checkinConfirmation.text,
		guest: store.guest.info
	}
})(View);