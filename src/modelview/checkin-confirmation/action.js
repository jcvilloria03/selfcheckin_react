import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			stayDetailsLabel: data.stay_details_label,
			checkinLabel: data.check_in_label,
			checkoutLabel: data.checkout_label,
			nightsLabel: data.nights_label,
			guestLabel: data.guest_label,
			signatureInstruction: data.signature_instruction,
			packageLabel: data.package_label,
			roomTypeLabel: data.room_type,
			preferencesLabel: data.preferences_label,
			extraPreferencesLabel: data.extra_preferences_label,
			termsConditionLabel: data.terms_condition_label,
			promotionalLabel: data.promotional_label,
			errorSignature: data.error_signature,
			errorTerms: data.error_terms
		}
	}
}

export default Action;