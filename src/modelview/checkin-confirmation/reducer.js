import {
	TEXT_SET
} from "./constant";

const initialState = {
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return {
		...state,
		text:{
			title: action.title,
			stayDetailsLabel: action.stayDetailsLabel,
			checkinLabel: action.checkinLabel,
			checkoutLabel: action.checkoutLabel,
			nightsLabel: action.nightsLabel,
			guestLabel: action.guestLabel,
			signatureInstruction: action.signatureInstruction,
			packageLabel: action.packageLabel,
			roomTypeLabel: action.roomTypeLabel,
			preferencesLabel: action.preferencesLabel,
			extraPreferencesLabel: action.extraPreferencesLabel,
			termsConditionLabel: action.termsConditionLabel,
			promotionalLabel: action.promotionalLabel,
			errorSignature: action.errorSignature,
			errorTerms: action.errorTerms,
		}
	}
}

export default reducer;