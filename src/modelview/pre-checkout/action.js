import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			or: data.or,
			firstnamePlaceholder: data.firstname,
			lastnamePlaceholder: data.lastname,
			confirmationNumberPlaceholder: data.confirmation_placeholder,
			error: data.error
		}
	}
}

export default Action;