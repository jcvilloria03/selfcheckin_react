import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import Loader from 'react-loader-spinner'
import Cookies from 'universal-cookie';

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action";

import {
	Footer,
	Textbox
} from "../../components"

import "./style.css";

const cookies = new Cookies();

class View extends Component {
	page = "pal-pre-checkout";

	state = {
		loader: false,
		error: null,
		firstname: "",
		lastname: ""
	}

	componentWillMount(){
		this.props.dispatch(GuestAction.unsetGuestInfoFull());
		cookies.remove("auth_token")
	}

	async componentDidMount(){
		let requests = [
			{url: "interface", body: {
				name: "pre_checkout"
			}}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))
	}

	handleContinueClick(){
		this.setState({loader: true});

		setTimeout(async ()=>{
			let request;

			if (this.state.firstname !== "" && this.state.lastname !== "" && this.state.firstname && this.state.lastname) {
				request = [
					{
						url: "booking", 
						method: "post",
						body: {
							firstName: this.state.firstname,
							lastName: this.state.lastname
						}
					}
				];
			}

			if (this.state.confNo !== "" && this.state.confNo) {
				request = [
					{
						url: "booking", 
						method: "post",
						body: {
							confNo: this.state.confNo,
							checkout: 1
						}
					}
				];
			}

			if (!request){
				this.setState({loader: false});
				return;
			}

			const [guest] = await Api.fetch(request);
			
			if (!guest.status){
				this.manageError(true);
				return;
			}

				
			this.props.dispatch(GuestAction.setGuestInfo(guest));
			this.props.history.push("checkout/bill")
			return;
		}, 300);
	}

	handleTextChange(e){
		const input = e.currentTarget;

		this.setState({ [input.getAttribute("name")]: input.value });
	}

	manageError(error){
		this.setState({
			loader: false,
			error
		});
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			loader,
  			error
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={`${this.page}`}>
	    		<Header key={this.page}>
	    			<title>Pre Checkout</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<p className={`pal-error${error ? " active" : ""}`} onClick={this.manageError.bind(this, false)}>
	    				{ text.error }
	    			</p>

		    		<Textbox type={"text"} numberOnly={true} handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)} name={"confNo"} className={"align-center"} placeholder={text.confirmationNumberPlaceholder}/>
		    		<h2 className={"font-normal color-primary"}>{text.or}</h2>
		    		<Textbox handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)} value={this.state.firstname} name={"firstname"} type="text" className={"align-center"} placeholder={text.firstnamePlaceholder}/>
		    		<Textbox handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)} value={this.state.lastname} name={"lastname"} type="text" className={"align-center"} placeholder={text.lastnamePlaceholder}/>
	    		</div>

	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		text: store.preCheckout.text
	}
})(View);