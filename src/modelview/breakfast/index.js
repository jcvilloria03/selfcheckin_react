import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import {
	Button,
	Footer,
	Textbox
} from "../../components"

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action"

import "./style.css";

class View extends Component {
	page = "pal-breakfast";

	state = {
		quantity: 0
	}

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "breakfast"
			}},
			{url: "interfaceimage", body: {
				name: "breakfast"
			}}
		];

		const [text, icon] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))
		this.props.dispatch(Action.setIcon(icon))
	}

	handleQuantityClick(action){
		let quantity = this.state.quantity;
		quantity += action === "add" ? 1 :
			quantity === 0 ? 0 : -1
			
		this.setState({
			quantity
		})
	}

	async handleContinueClick(){
		await this.props.dispatch(GuestAction.setBreakfastIncluded(this.state.quantity))
		this.props.history.push("/checkin/confirmation")
	}

  	render(){
  		const {
  			history,
  			icon,
  			text
  		} = this.props

  		const {
  			quantity
  		} = this.state
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Breakfast</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
	    			<h2 className={"font-normal"}>{text.header}</h2>

	    			<div className={"image-wrapper"}>
	    				{icon.breakfast ? (<img alt={"Breakfast"} src={icon.breakfast} />) : null}
	    			</div>

	    			<div className={"table"}>
	    				<div>
	    					<Button handleClick={this.handleQuantityClick.bind(this, "subtract")}>-</Button>
	    				</div>
	    				<div>
	    					<Textbox className={"align-center"} value={quantity} readOnly={true} type={"text"}/>
	    				</div>
	    				<div>
	    					<Button handleClick={this.handleQuantityClick.bind(this, "add")}>+</Button>
	    				</div>
	    			</div>
	    		</div>

	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		icon: store.breakfast.icon,
		text: store.breakfast.text
	}
})(View);