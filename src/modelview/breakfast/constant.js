const ICON_SET = "breakfast/ICON_SET"
const TEXT_SET = "breakfast/TEXT_SET"

export {
	ICON_SET,
	TEXT_SET
}