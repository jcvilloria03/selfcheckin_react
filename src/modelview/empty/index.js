import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import {
	Footer
} from "../../components"

import "./style.css";

class View extends Component {
	page = "pal-empty";


  	render(){
  		const {
  			history
  		} = this.props
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Front Page</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1>Empty Stuff</h1>
	    		</div>

	    		<Footer history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {

	}
})(View);