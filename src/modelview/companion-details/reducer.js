import {
	INDEX_INCREMENT
} from "./constant";

const initialState = {
	index: 0
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case INDEX_INCREMENT:
			return {
				...state,
				index: (state.index + 1)
			}
		default:
			return state;
	}
}

export default reducer;