import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"
import iso from 'iso-3166-1';

import {
	DatePicker,
	Dropdown,
	Footer,
	Textbox
} from "../../components"

import {
	timezone
} from "../../env"

import Countries from "../../country"
import GuestAction from "../guest/action"
import Loader from 'react-loader-spinner'
import Validate from "../../validation"

import "./style.css";

class View extends Component {
	page = "pal-guest-details";

	state = {
		genderOptions: [
			{value: "MALE", label: "Male"},
			{value: "FEMALE", label: "Female"}
		],
		loader: false,
		textForms: {},
		error: {
			status: false,
			text: ""
		}
	}

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		const companion = this.props.companionList[Object.keys(this.props.companionList)[this.props.index]]

		await this.setState({
			textForms: {
				birthdate: companion.birthdate ? moment.tz(companion.birthdate, timezone).format("YYYY-MM-DD") : null,
				country: companion.address.country ? {value: companion.address.country, label: iso.whereAlpha2(companion.address.country).country} : null,
				firstname: companion.firstname ? this.setFirstLetterToUppercase(companion.firstname) : null,
				lastname: companion.lastname ? this.setFirstLetterToUppercase(companion.lastname) : null,
				passportNumber: companion.passport.number,
				mobileNumber: companion.mobileNumber,
				email: companion.email,
				street: companion.address.street,
				state: companion.address.state,
				postal: companion.address.postal,
				city: companion.address.city,
				expiration: companion.passport.expiration ? moment.tz(companion.passport.expiration, timezone).format("YYYY-MM-DD") : null,
				issuingAuthority: companion.passport.issuingAuthority ? {value: companion.passport.issuingAuthority, label: iso.whereAlpha2(companion.passport.issuingAuthority).country} : null,
				gender: companion.gender ? (companion.gender === "MALE" ? {value: "MALE", label: "Male"} : {value: "FEMALE", label: "Female"}) : null
			}
		});

		this.setInitialValue([
			"mobileNumber",
			"email",
			"street",
			"state",
			"postal",
			"city",
			"firstname",
			"lastname",
			"passportNumber",
		]);

		const element = document.querySelector(`.issuingAuthority`)
		element.addEventListener("click", ()=>{
			const wrapper = document.querySelector(`.animate-wrapper`)
			wrapper.scrollTop = wrapper.scrollHeight;
		})
	}

	handleContinueClick(){
		const textForms = {
			...this.state.textForms
		}

		textForms.country = textForms.country ? textForms.country.value : ""
		textForms.issuingAuthority = textForms.issuingAuthority ? textForms.issuingAuthority.value : ""
		textForms.gender = textForms.gender ? textForms.gender.value : ""

		let required = Object.keys(textForms).map(key=>{
			return {
				key: key,
				value: textForms[key] ? textForms[key] : ""
			}
		}).filter(data=>(data.key !== "postal" && data.key !== "state"))

		required = Validate.required(required)

		if (required.length !== 0) {
			this.setErrorInput(required)
			this.manageError(true, this.props.text.errorRequired)
			return;
		}

		if (!Validate.email(textForms.email)) {
			this.setErrorInput(["email"])
			this.manageError(true, this.props.text.errorEmail)
			return;
		}

		this.setState({ loader: true});

		setTimeout(async ()=>{
			const companion = {
				address:{
					street: textForms.street,
					country: textForms.country,
					city: textForms.city,
					postal: textForms.postal,
					state: textForms.state,
				},
				birthdate: textForms.birthdate,
				email: textForms.email,
				firstname: textForms.firstname,
				lastname: textForms.lastname,
				mobileNumber: textForms.mobileNumber,
				gender: textForms.gender,
				passport: {
					number: textForms.passportNumber,
					expiration: textForms.expiration,
					issuingAuthority: textForms.issuingAuthority
				},
			}

			await this.props.dispatch(GuestAction.updateCompanion(companion))
			this.props.history.push("/companion/signature")
		}, 300)
	}

	handleBirthdateChange(date){
		const element = document.querySelector(`.birthdate`)
	    element.className = element.className.replace(/ error/g, "")

		this.setState({
			textForms: {
				...this.state.textForms,
				birthdate: moment.tz(date, timezone).format("YYYY-MM-DD")
			}
		});
	}

	handleCountryChange(country){
		const element = document.querySelector(`.country`)
	    element.className = element.className.replace(/ error/g, "")
		this.setState({
			textForms: {
				...this.state.textForms,
				country
			}
		});
	}

	handleExpirationChange(date){
		const element = document.querySelector(`.expiration`)
	    element.className = element.className.replace(/ error/g, "")

		this.setState({
			textForms: {
				...this.state.textForms,
				expiration: moment.tz(date, timezone).format("YYYY-MM-DD")
			}
		});
	}

	handleGenderChange(gender){
		const element = document.querySelector(`.gender`)
	    element.className = element.className.replace(/ error/g, "")
		this.setState({
			textForms: {
				...this.state.textForms,
				gender
			}
		});
	}

	handleIssuingAuthorityChange(issuingAuthority){
		const element = document.querySelector(`.issuingAuthority`)
	    element.className = element.className.replace(/ error/g, "")
		this.setState({
			textForms: {
				...this.state.textForms,
				issuingAuthority
			}
		});
	}

	handleTextChange(e){
		const input = e.currentTarget;
	    input.className = input.className.replace(/ error/g, "")

		this.setState({ 
			textForms: {
				...this.state.textForms,
				[input.getAttribute("name")]: input.value
			}
		});
	}

	manageError(error, message){
		this.setState({
			error: {
				status: error,
				text: message
			}
		})
	}

	setErrorInput(data){
		for(let i = 0; i < data.length; i++){
			let element;
			switch(data[i]){
				case "birthdate":
				case "expiration":
				case "gender":
				case "country":
				case "issuingAuthority":
				element = document.querySelector(`.${data[i]}`)
				break;

				default:
				element = document.querySelector(`[name="${data[i]}"]`)
				break;
			}

			element.className += " error"
		}
	}

	setFirstLetterToUppercase(str) {
	   const splitStr = str.toLowerCase().split(' ');
	   for (let i = 0; i < splitStr.length; i++) {
	       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
	   }

	   return splitStr.join(' '); 
	}

	setInitialValue(data){
		for(let i = 0; i < data.length; i++){
			const element = document.querySelector(`[name="${data[i]}"]`);
			element.value = this.state.textForms[data[i]] ? this.state.textForms[data[i]] : ""
		}
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			error,
  			loader,
  			genderOptions,
  			textForms,
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Guest Details</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<p className={`pal-error${error.status ? " active" : ""}`} onClick={this.manageError.bind(this, false, "")}>
	    				{ error.text }
	    			</p>

	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
	    			<div className={"form align-left"}>
	    				<h2 className={"font-normal"}>{text.personal}</h2>
	    				<Textbox type="text" className={"align-center"} placeholder={text.firstname} name={"firstname"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<Textbox type="text" className={"align-center"} placeholder={text.lastname} name={"lastname"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<DatePicker className={"birthdate"} maxDate={new Date(moment.tz(timezone).subtract("16", "years"))} placeholder={text.birthdate} selected={textForms.birthdate ? new Date(textForms.birthdate) : null} handleChange={this.handleBirthdateChange.bind(this)}/>
	    				<Dropdown className={"drop-down align-center gender"} placeholder={text.gender} options={genderOptions} value={textForms.gender} handleChange={this.handleGenderChange.bind(this)}/>
	    				<Textbox type="text" className={"align-center"} placeholder={text.phoneNumber} name={"mobileNumber"} handleChange={this.handleTextChange.bind(this)} numberOnly={true} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<Textbox type="text" className={"align-center"} placeholder={text.email} name={"email"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<br />
	    				<br />
	    				<h2 className={"font-normal"}>{text.address}</h2>
	    				<Textbox type="text" className={"align-center"} placeholder={text.street} name={"street"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<Textbox type="text" className={"align-center"} placeholder={text.state} name={"state"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<Textbox type="text" className={"align-center"} placeholder={text.postalCode} name={"postal"} handleChange={this.handleTextChange.bind(this)} numberOnly={true} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<Textbox type="text" className={"align-center"} placeholder={text.city} name={"city"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<Dropdown className={"drop-down align-center country"} placeholder={text.country} options={Countries} value={textForms.country} handleChange={this.handleCountryChange.bind(this)}/>
	    				<br />
	    				<br />
	    				<h2 className={"font-normal"}>{text.passport}</h2>
	    				<Textbox type="text" className={"align-center"} placeholder={text.passportNumber} name={"passportNumber"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    				<DatePicker className={"expiration"} minDate={new Date()} placeholder={text.expiration} selected={textForms.expiration ? new Date(textForms.expiration) : null} handleChange={this.handleExpirationChange.bind(this)}/>
	    				<Dropdown className={"drop-down align-center issuingAuthority"} placeholder={text.issuing} options={Countries} value={textForms.issuingAuthority} handleChange={this.handleIssuingAuthorityChange.bind(this)}/>
	    			</div>
	    		</div>

	    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		companionList: store.guest.info.companionList,
		index: store.companionDetails.index,
		text: store.guest.text,
	}
})(View);