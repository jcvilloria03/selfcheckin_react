import {
	INDEX_INCREMENT
} from "./constant";

class Action{
	static incrementIndex(){
		return {
			type: INDEX_INCREMENT
		}
	}
}

export default Action;