import {
	BREAKFAST_SET,
	COMPANION_ADD_VIA_SCAN,
	COMPANION_UPDATE,
	DETAILS_SET,
	EARLY_CHECKIN_SET,
	GUEST_INFO,
	GUEST_INFO_VIA_SCAN,
	GUEST_INFO_VIA_PICKUP,
	LATE_CHECKOUT_SET,
	ROOM_EXTRA_PREFERENCES,
	ROOM_PREFERENCES,
	ROOM_UPGRADE,
	TEXT_SET,
	UNSET_GUEST_INFO,
	UNSET_ROOM_UPGRADE
} from "./constant";

class Action{
	static addCompanionViaScan(data){
		return {
			type: COMPANION_ADD_VIA_SCAN,
			birthdate: data.birthdate,
			country: data.country,
			firstname: data.firstname,
			gender: data.gender,
			lastname: data.lastname,
			photo: data.passportPhoto,
			passport: {
				expiration: data.passportExpiration,
				issuingAuthority: data.passportIssuing,
				number: data.passportNumber,
				photo: data.passportImage
			}
		}
	}

	static setBreakfastIncluded(breakfastIncluded){
		return {
			type: BREAKFAST_SET,
			breakfastIncluded
		}
	}

	static setDetails(data){
		return {
			type: DETAILS_SET,
			address: {
				street: data.street,
				city: data.city,
				postal: data.postal,
				state: data.state,
				country: data.country
			},
			birthdate: data.birthdate,
			email: data.email,
			firstname: data.firstname,
			gender: data.gender,
			lastname: data.lastname,
			mobileNumber: data.mobileNumber,
			passport: {
				number: data.passportNumber,
				expiration: data.expiration,
				issuingAuthority: data.issuingAuthority
			}
		}
	}

	static setEarlyCheckin(isEarly){
		return {
			type: EARLY_CHECKIN_SET,
			isEarly
		}
	}

	static setGuestInfo(data){
		return {
			type: GUEST_INFO,
			...data
		}
	}

	static setGuestInfoViaPassportScan(data){
		return {
			type: GUEST_INFO_VIA_SCAN,
			birthdate: data.birthdate,
			country: data.country,
			firstname: data.firstname,
			gender: data.gender,
			lastname: data.lastname,
			photo: data.passportPhoto,
			passport: {
				expiration: data.passportExpiration,
				issuingAuthority: data.passportIssuing,
				number: data.passportNumber,
				photo: data.passportImage
			}
		}
	}

	static setGuestInfoViaPickup(data){
		return {
			type: GUEST_INFO_VIA_PICKUP,
			reservationId: data.reservationId,
			guestDetails: {
				firstname: data.firstName,
				lastname: data.lastName,
				roomDetails: {
					roomNumber: data.roomNumber
				},
				stayDetails: {
					arrivalDate: data.arrivalDate,
					departureDate: data.departureDate
				}
			}
		}
	}

	static setLateCheckout(lateCheckout){
		return {
			type: LATE_CHECKOUT_SET,
			lateCheckout
		}
	}

	static setRoomPreferences(data, action){
		return{
			type: ROOM_PREFERENCES,
			preferences: data,
			action
		}
	}

	static setRoomExtraPreferences(data, action){
		return{
			type: ROOM_EXTRA_PREFERENCES,
			extraPreferences: data,
			action
		}
	}

	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			personal: data.personal,
			address: data.address,
			phoneNumber: data.phone_number,
			email: data.email,
			street: data.street,
			state: data.state,
			postalCode: data.postal_code,
			city: data.city,
			errorRequired: data.error_required,
			errorEmail: data.error_email,
			passport: data.passport,
			firstname: data.firstname_placeholder,
			lastname: data.lastname_placeholder,
			birthdate: data.birthday_placeholder,
			country: data.country_placeholder,
			gender: data.gender_placeholder,
			passportNumber: data.passport_placeholder,
			expiration: data.expiration_placeholder,
			issuing: data.issuing_placeholder,
		}
	}

	static unsetGuestInfoFull(){
		return {
			type: UNSET_GUEST_INFO
		}
	}

	static unsetRoomUpgrade(){
		return {
			type: UNSET_ROOM_UPGRADE
		}
	}

	static updateCompanion(data){
		return {
			type: COMPANION_UPDATE,
			data
		}
	}

	static upgradeRoom(data){
		return {
			type: ROOM_UPGRADE,
			roomNumber: data.roomnumber,
			roomType: data.name,
			roomTypeCode: data.roomtype
		}
	}
}

export default Action;