import {
	BREAKFAST_SET,
	COMPANION_ADD_VIA_SCAN,
	COMPANION_UPDATE,
	DETAILS_SET,
	EARLY_CHECKIN_SET,
	GUEST_INFO,
	GUEST_INFO_VIA_SCAN,
	GUEST_INFO_VIA_PICKUP,
	LATE_CHECKOUT_SET,
	ROOM_EXTRA_PREFERENCES,
	ROOM_PREFERENCES,
	ROOM_UPGRADE,
	TEXT_SET,
	UNSET_GUEST_INFO,
	UNSET_ROOM_UPGRADE,
} from "./constant";

const initialState = {
	text: {},
	info: {
		companionList: {},
		confirmationNumber: null,
		reservationId: null,
		guestDetails: {
			birthdate: null,
			email: null,
			firstname: "",
			gender: null,
			lastname: "",
			mobileNumber: null,
			photo: null,
			profileId: null,
			address: {
				id: null,
				city: null,
				country: null,
				postal: null,
				state: null,
				street: null
			},
			passport: {
				expiration: null,
				issuingAuthority: null,
				number: null,
				photo: null
			},
			roomDetails: {
				extraPreferences: {},
				preferences: {},
				roomNumber: null,
				roomNumberPrev: null,
				roomPackage: [],
				roomType: null,
				roomTypePrev: null,
				roomTypeCode: null,
				roomTypeCodePrev: null
			},
			stayDetails: {
				adultCount: 0,
				allowEarlyCheckin: null,
				allowLateCheckout: null,
				arrivalDate: null,
				breakfast: null,
				childCount: null,
				departureDate: null,
				earlyCheckin: null,
				lateCheckout: null,
				noOfNights: null,
				rateCode: null,
			}
		}
	}
}

const reducer = (state = initialState, action) => {
	switch(action.type){
		case BREAKFAST_SET:
			return setBreakfastIncluded(state, action)
		case COMPANION_ADD_VIA_SCAN:
			return addCompanionViaScan(state, action)
		case COMPANION_UPDATE:
			return updateCompanion(state, action)
		case DETAILS_SET:
			return setDetails(state, action)
		case EARLY_CHECKIN_SET:
			return setEarlyCheckin(state, action);
		case GUEST_INFO:
			return setGuestInfo(state, action);
		case GUEST_INFO_VIA_SCAN:
			return setGuestInfoViaPassportScan(state, action);
		case GUEST_INFO_VIA_PICKUP:
			return setGuestInfoViaPickup(state, action)
		case LATE_CHECKOUT_SET:
			return setLateCheckout(state, action)
		case ROOM_EXTRA_PREFERENCES:
			return setRoomExtraPreferences(state, action);
		case ROOM_PREFERENCES:
			return setRoomPreferences(state, action)
		case ROOM_UPGRADE:
			return upgradeRoom(state, action)
		case TEXT_SET:
			return setText(state, action);
		case UNSET_GUEST_INFO:
			return initialState;
		case UNSET_ROOM_UPGRADE:
			return unsetRoomUpgrade(state, action)
		default:
			return state;
	}
}

function addCompanionViaScan(state, action){
	return {
		...state,
		info: {
			...state.info,
			companionList: {
				...state.info.companionList,
				[action.passport.number]: {
					birthdate: action.birthdate,
					firstname: action.firstname,
					gender: action.gender,
					lastname: action.lastname,
					passport: action.passport,
					photo: action.photo,
					address: {
						country: action.country
					},
				}
			},
			guestDetails: {
				...state.info.guestDetails,
				stayDetails: {
					...state.info.guestDetails.stayDetails,
					adultCount: (state.info.guestDetails.stayDetails.adultCount + 1)
				}
			}
		}
	}
}

function setBreakfastIncluded(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				stayDetails: {
					...state.info.guestDetails.stayDetails,
					breakfast: action.breakfastIncluded
				}
			}
		}
	}
}

function setDetails(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				birthdate: action.birthdate,
				email: action.email,
				firstname: action.firstname,
				gender: action.gender,
				lastname: action.lastname,
				mobileNumber: action.mobileNumber,
				address: {
					...state.info.guestDetails.address,
					...action.address
				},
				passport: {
					...state.info.guestDetails.passport,
					...action.passport
				}
			}
		}
	}
}

function setEarlyCheckin(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				stayDetails: {
					...state.info.guestDetails.stayDetails,
					earlyCheckin: action.isEarly
				}
			}
		}
	}
}

function setGuestInfoViaPassportScan(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				birthdate: action.birthdate,
				firstname: action.firstname,
				gender: action.gender,
				lastname: action.lastname,
				passport: action.passport,
				photo: action.photo,
				address: {
					...state.info.guestDetails.address,
					country: action.country
				},
				stayDetails: {
					...state.info.guestDetails.stayDetails,
					adultCount: (state.info.guestDetails.stayDetails.adultCount + 1)
				}
			}
		}
	}
}

function setGuestInfoViaPickup(state, action){
	return {
		...state,
		info: {
			...state.info,
			...action
		}
	}
}

function setGuestInfo(state, action){
	return {
		...state,
		info: {
			...state.info,
			confirmationNumber: action.confNumber,
			reservationId: action.reservationId,
			guestDetails: {
				...state.info.guestDetails,
				birthdate: state.info.guestDetails.birthdate ? state.info.guestDetails.birthdate : action.birthdate,
				email: action.email,
				firstname: state.info.guestDetails.firstname ? state.info.guestDetails.firstname : action.firstName,
				gender: state.info.guestDetails.gender ? state.info.guestDetails.gender : action.gender,
				lastname: state.info.guestDetails.lastname ? state.info.guestDetails.lastname : action.lastName,
				mobileNumber: action.mobileNumber,
				profileId: action.profileId,
				address: {
					...state.info.guestDetails.address,
					id: action.addressId,
					city: action.city,
					country: state.info.guestDetails.country ? state.info.guestDetails.country : action.country,
					postal: action.postal,
					state: action.state,
					street: action.address
				},
				roomDetails: {
					...state.info.guestDetails.roomDetails,
					roomNumber: action.roomNumber,
					roomNumberPrev: action.roomNumber,
					roomPackage: action.roomPackage,
					roomType: action.roomType,
					roomTypePrev: action.roomType,
					roomTypeCode: action.roomTypeCode,
					roomTypeCodePrev: action.roomTypeCode,
				},
				stayDetails: {
					...state.info.guestDetails.stayDetails,
					allowEarlyCheckin: action.earlyCheckin,
					allowLateCheckout: action.lateCheckout,
					allowBreakfast: action.breakfast === "1",
					arrivalDate: action.arrivalDate,
					breakfast: action.breakfastIncluded,
					childCount: action.childCount,
					departureDate: action.departureDate,
					noOfNights: action.nights,
					rateCode: action.rateCode,
				}
			}
		}
	}
}

function setLateCheckout(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				stayDetails: {
					...state.info.guestDetails.stayDetails,
					lateCheckout: action.lateCheckout
				}
			}
		}
	}
}

function setRoomPreferences(state, action){
	const preferences = state.info.guestDetails.roomDetails.preferences
	if (action.action === "add")
		preferences[action.preferences.code] = {
			price: action.preferences.price,
			name: action.preferences.name
		}
	else{
		if (action.preferences.code in preferences)
			delete preferences[action.preferences.code]
	}

	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				roomDetails: {
					...state.info.guestDetails.roomDetails,
					preferences
				}
			}
		}
	}
}

function setRoomExtraPreferences(state, action){
	const extraPreferences = state.info.guestDetails.roomDetails.extraPreferences
	if (action.action === "add")
		extraPreferences[action.extraPreferences.code] = {
			price: action.extraPreferences.price,
			name: action.extraPreferences.name
		}
	else{
		if (action.extraPreferences.code in extraPreferences)
			delete extraPreferences[action.extraPreferences.code]
	}

	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				roomDetails: {
					...state.info.guestDetails.roomDetails,
					extraPreferences
				}
			}
		}
	}
}

function setText(state, action){
	return{
		...state,
		text: {
			title: action.title,
			personal: action.personal,
			address: action.address,
			phoneNumber: action.phoneNumber,
			email: action.email,
			street: action.street,
			state: action.state,
			postalCode: action.postalCode,
			city: action.city,
			errorRequired: action.errorRequired,
			errorEmail: action.errorEmail,
			passport: action.passport,
			firstname: action.firstname,
			lastname: action.lastname,
			birthdate: action.birthdate,
			country: action.country,
			gender: action.gender,
			passportNumber: action.passportNumber,
			expiration: action.expiration,
			issuing: action.issuing,
		}
	}
}

function unsetRoomUpgrade(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				roomDetails: {
					...state.info.guestDetails.roomDetails,
					roomTypeCode: state.info.guestDetails.roomDetails.roomTypeCodePrev,
					roomType: state.info.guestDetails.roomDetails.roomTypePrev,
					roomNumber: state.info.guestDetails.roomDetails.roomNumberPrev,
					preferences: {},
					extraPreferences: {}
				}
			}
		}
	}
}

function updateCompanion(state, action){
	return {
		...state,
		info: {
			...state.info,
			companionList: {
				...state.info.companionList,
				[action.data.passport.number]: action.data
			}
		}
	}
}

function upgradeRoom(state, action){
	return {
		...state,
		info: {
			...state.info,
			guestDetails: {
				...state.info.guestDetails,
				roomDetails: {
					...state.info.guestDetails.roomDetails,
					roomNumber: action.roomNumber,
					roomType: action.roomType,
					roomTypeCode: action.roomTypeCode,
				}
			}
		}
	}
}

export default reducer;