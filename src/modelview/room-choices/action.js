import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			selectedInstruction: data.selected_instruction,
			choicesInstruction: data.choices_instruction
		}
	}
}

export default Action;