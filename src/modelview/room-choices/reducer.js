import {
	TEXT_SET
} from "./constant";

const initialState = {
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return {
		...state,
		text:{
			title: action.title,
			selectedInstruction: action.selectedInstruction,
			choicesInstruction: action.choicesInstruction,
		}
	}
}

export default reducer;