import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import {
	Footer
} from "../../components"

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action"
import RoomDescriptionAction from "../room-description/action"

import "./style.css";

class View extends Component {
	page = "pal-room-choices";

	state = {
		choices: [],
		selected: {}
	}

	async componentDidMount(){
		if (!this.props.passportNumber && !this.props.roomTypeCode) {
			this.props.history.push("/pre-checkin");
			return;
		}

		let requests = [
			{
				url: "interface",
				body: {
					name: "room_choices"
				}
			}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text));
		this.props.dispatch(GuestAction.unsetRoomUpgrade());

		requests = [
			{
				url: "roomupsell",
				method: "post",
				body: {
					code: this.props.roomTypeCode
				}
			}
		];

		const [roomResponse] = await Api.fetch(requests);
		const choices = roomResponse.other_room_type.map(data=>{
			return {
				code: data.code,
				name: data.name,
				images: data.images,
				mainImage: data.main_image,
				description: data.room_description,
				price: data.upgrade_price,
			}
		})

		this.setState({
			selected: {
				code: roomResponse.current_room_type.code,
				name: roomResponse.current_room_type.name,
				images: roomResponse.current_room_type.images,
				mainImage: roomResponse.current_room_type.main_image,
				description: roomResponse.current_room_type.room_description,
				price: roomResponse.current_room_type.upgrade_price,
			},

			choices
		})
	}

	handleContinueClick(){
		this.props.history.push(this.props.allowLateCheckout === "1" ? "/late-checkout" : 
			this.props.allowBreakfast > 0 ? "/checkin/confirmation" : "/breakfast")
	}

	async handleRoomClick(data, selected){
		data.images.splice(0,0,data.mainImage);
		data.selected = selected;

		await this.props.dispatch(RoomDescriptionAction.setRoomDetails(data))
		this.props.history.push("/room/description")
	}


  	render(){
  		const {
  			history,
  			roomTypeCode,
  			text
  		} = this.props

  		const {
  			selected,
  			choices
  		} = this.state
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Room Preferences</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{`${text.title}`.replace(/\[room\]/g, roomTypeCode)}</h1>

	    			<div className={"image-wrapper"} onClick={this.handleRoomClick.bind(this, selected, true)}>
	    				{selected.mainImage ? (<img alt={selected.name} src={selected.mainImage}/>) : null}
	    				{selected.name ? (<p className={"color-white"}>{selected.name}</p>) : null}
	    			</div>
	    			<p className={"font-italic color-gray align-center email-instuction"}>{text.selectedInstruction}</p>
	    			<br/>
	    			<br/>
	    			<div className={"room-wrapper align-left"}>
	    				{
	    					choices.map(data=>(
	    						<div key={data.name} className={"room"} onClick={this.handleRoomClick.bind(this, data, false)}>
	    							<img alt={data.name} src={data.mainImage}/>
	    							<p className={"color-white align-center"}>{data.name}</p>
	    						</div>
	    					))
	    				}
	    			</div>
	    			<p className={"font-italic color-gray align-center email-instuction"}>{text.choicesInstruction}</p>
	    		</div>

	    		<Footer history={history} rightText={"Skip"} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		allowLateCheckout: store.guest.info.guestDetails.stayDetails.allowLateCheckout,
		allowBreakfast: store.guest.info.guestDetails.stayDetails.allowBreakfast,
		passportNumber: store.guest.info.guestDetails.passport.number,
		roomTypeCode: store.guest.info.guestDetails.roomDetails.roomTypeCode,
		text: store.roomChoices.text,
	}
})(View);