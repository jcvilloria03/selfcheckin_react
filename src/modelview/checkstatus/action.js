import {
	ICON_SET,
	TEXT_SET
} from "./constant";

class Action{
	static setIcon(data){
		return {
			type: ICON_SET,
			checkin: data.check_in,
			checkout: data.check_out,
			pickupkey: data.pickup_key
		}
	}
	
	static setText(data){
		return {
			type: TEXT_SET,
			checkin: data.checkin,
			checkout: data.checkout,
			pickupkey: data.pickupkey
		}
	}
}

export default Action;