import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import Action from "./action"
import Api from "../../api"

import {
	ButtonImage,
	Footer
} from "../../components"

import "./style.css";

class View extends Component {
	page = "pal-checkstatus";


	async componentDidMount(){
		let requests = [
			{url: "interface", body: {
				name: "check_status"
			}},
			{url: "interfaceimage", body: {
				name: "check_status"
			}}
		];

		const [text, icon] = await Api.fetch(requests);
		this.props.dispatch(Action.setIcon(icon));
		this.props.dispatch(Action.setText(text));
	}

	handleCheckinClick(){
		this.props.history.push("passport-scan");
	}

	handleCheckoutClick(){
		this.props.history.push("pre-checkout");
	}

	handlePickUpKeyClick(){
		this.props.history.push("pickup-key");
	}

  	render(){
  		const {
  			checkin,
  			checkout,
  			pickupkey,
  			history
  		} = this.props
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Check Status</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<div className={"pickup-key"}>
		    			<ButtonImage handleClick={this.handlePickUpKeyClick.bind(this)} icon={pickupkey.icon} />
		    			<p>{pickupkey.text}</p>
		    		</div>

	    			<div className={"check-in"}>
		    			<ButtonImage handleClick={this.handleCheckinClick.bind(this)} icon={checkin.icon} />
		    			<p>{checkin.text}</p>
		    		</div>

		    		<div className={"check-out"}>
		    			<ButtonImage handleClick={this.handleCheckoutClick.bind(this)} icon={checkout.icon} />
		    			<p>{checkout.text}</p>
		    		</div>
	    		</div>

	    		<Footer renderRight={false} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		pickupkey: store.checkStatus.pickupkey,
		checkin: store.checkStatus.checkin,
		checkout: store.checkStatus.checkout
	}
})(View);