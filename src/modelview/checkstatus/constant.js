const ICON_SET = "check-status/ICON_SET"
const TEXT_SET = "check-status/TEXT_SET"

export {
	ICON_SET,
	TEXT_SET
}