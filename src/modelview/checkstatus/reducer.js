import {
	ICON_SET,
	TEXT_SET
} from "./constant";

const initialState = {
	checkin: {
		icon: null,
		text: null
	},
	checkout: {
		icon: null,
		text: null
	},
	pickupkey: {
		icon: null,
		text: null
	}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case ICON_SET:
			return setIcon(state, action);
		case TEXT_SET:
			return setText(state, action);
		default:
			return state;
	}
}

function setIcon(state, action){
	return {
		...state,
		checkin: {
			...state.checkin,
			icon: action.checkin
		},
		checkout: {
			...state.checkout,
			icon: action.checkout
		},
		pickupkey: {
			...state.pickupkey,
			icon: action.pickupkey
		},
	}
}

function setText(state, action){
	return {
		...state,
		checkin: {
			...state.checkin,
			text: action.checkin
		},
		checkout: {
			...state.checkout,
			text: action.checkout
		},
		pickupkey: {
			...state.pickupkey,
			text: action.pickupkey
		},
	}
}

export default reducer;