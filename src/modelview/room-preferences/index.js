import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import Loader from 'react-loader-spinner'

import {
	ButtonImage,
	Footer
} from "../../components"

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action"

import "./style.css";

class View extends Component {
	page = "pal-room-preferences";

	state = {
		smoking: [],
		misc: [],
		bedtype: [],
		preferences: [],
		extraPreferences: [],
		upgrade: false,
		loader: true,
	}

	async componentDidMount(){
		if (!this.props.passportNumber && !this.props.roomTypeCode) {
			this.props.history.push("/pre-checkin");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "room_preferences"
			}}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))

		requests = [
			{url: "roombypreferencesbytype", method: "post", body: {
				roomType: this.props.roomTypeCode,
				arrivalDate: this.props.arrivalDate,
				departureDate: this.props.departureDate
			}}
		];

		const [preferences] = await Api.fetch(requests);

		this.setState({
			smoking: preferences.smoking ? preferences.smoking : [],
			bedtype: preferences.bedtype ? preferences.bedtype : [],
			misc: preferences.misc ? preferences.misc : [],
			preferences: Object.keys(this.props.selectedPreferences),
			extraPreferences: Object.keys(this.props.selectedExtraPreferences),
			loader: false
		})
	}

	handlePreferencesClick(data){
		const preferences = this.state.preferences
		const index = preferences.indexOf(data.code)

		if (index > -1) {
			preferences.splice(index, 1)
			this.props.dispatch(GuestAction.setRoomPreferences(data, "subtract"))
		}else{
			preferences.push(data.code)
			this.props.dispatch(GuestAction.setRoomPreferences(data, "add"))
		}

		this.setState({
			preferences
		})

		this.fetchRoom();
	}

	handleExtraPreferencesClick(data){
		const extraPreferences = this.state.extraPreferences
		const index = extraPreferences.indexOf(data.code)

		if (index > -1) {
			extraPreferences.splice(index, 1)
			this.props.dispatch(GuestAction.setRoomExtraPreferences(data, "subtract"))
		}else{
			extraPreferences.push(data.code)
			this.props.dispatch(GuestAction.setRoomExtraPreferences(data, "add"))
		}

		this.setState({
			extraPreferences
		})

		this.fetchRoom();
	}

	async fetchRoom(){
		const requests = [
			{url: "roombypreferencesv2", method: "post", body: {
				roomType: this.props.roomTypeCode,
				arrivalDate: this.props.arrivalDate,
				departureDate: this.props.departureDate,
				preferences: this.state.preferences.toString(),
				extra_preferences: this.state.extraPreferences.toString()
			}}
		];

		const [room] = await Api.fetch(requests);
		this.setState({
			upgrade: room.status
		})
	}

	async handleContinueClick(){
		if (!this.state.skip) {
			const requests = [
				{url: "roombypreferencesv2", method: "post", body: {
					roomType: this.props.roomTypeCode,
					arrivalDate: this.props.arrivalDate,
					departureDate: this.props.departureDate,
					preferences: this.state.preferences.toString(),
					extra_preferences: this.state.extraPreferences.toString(),
					upgrade: 1,
					confNo: this.props.confirmationNumber
				}}
			];

			const [upgrade] = await Api.fetch(requests);
			upgrade["name"] = this.props.roomType
			await this.props.dispatch(GuestAction.upgradeRoom(upgrade))
		}

		this.props.history.push(this.props.allowLateCheckout === "1" ? "/late-checkout" : 
			this.props.allowBreakfast ? "/checkin/confirmation" : "/breakfast")
	}


  	render(){
  		const {
  			history,
  			selectedPreferences,
  			selectedExtraPreferences,
  			text
  		} = this.props

  		const {
  			smoking,
  			misc,
  			bedtype,
  			preferences,
  			extraPreferences,
  			upgrade,
  			loader
  		} = this.state

  		if (loader)
  		return(
  			<Loader className={"asd"} type={"ThreeDots"}/>
  		);

  		if (smoking.length < 1 && misc.length < 1 && bedtype.length < 1) {
  			return(
  				<div className={this.page + " no-preferences"}>
		    		<Header key={this.page}>
		    			<title>Room Preferences</title>
		    		</Header>

		    		<h1 className={"font-normal color-primary"}>{text.noPreferences}</h1>

		    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)}/>
		    	</div>
  			)
  		}
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Room Preferences</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<div className={"table"}>
	    				{smoking.length < 1 ? null : (
	    					<div>
	    						<h3>{text.required}</h3>
	    						<br/>
	    						{smoking.map(data=>(
			    					<div key={data.name}>
						    			<ButtonImage icon={data.image} className={preferences.indexOf(data.code) > -1 ? "clicked" : (data.code in selectedPreferences ? "clicked" : "")} handleClick={this.handlePreferencesClick.bind(this, data)}/>
						    			<p className={"align-center"}>{data.name}</p>
						    		</div>
			    				))}
			    			</div>
	    				)}

	    				{bedtype.length < 1 ? null : (
	    					<div>
	    						<h3>{text.required}</h3>
	    						<br/>
			    				{bedtype.map(data=>(
			    					<div key={data.name}>
						    			<ButtonImage icon={data.image} className={preferences.indexOf(data.code) > -1 ? "clicked" : (data.code in selectedPreferences ? "clicked" : "")} handleClick={this.handlePreferencesClick.bind(this, data)}/>
						    			<p className={"align-center"}>{data.name}</p>
						    		</div>
			    				))}
			    			</div>
	    				)}
	    			</div>

	    			<div className={"align-center views"}>
	    				<h3>{text.optional}</h3>
	    				<h3>{text.charges}</h3>
	    				<br/>
	    				{misc.map(data=>(
	    					<div key={data.name}>
				    			<ButtonImage icon={data.image} className={extraPreferences.indexOf(data.code) > -1 ? "clicked" : (data.code in selectedExtraPreferences ? "clicked" : "")} handleClick={this.handleExtraPreferencesClick.bind(this, data)}/>
				    			<p className={"align-center"}>{data.name}</p>
				    		</div>
	    				))}
	    			</div>
	    		</div>

	    		<Footer history={history} renderRight={upgrade ? upgrade : (preferences.length === 0 && extraPreferences.length === 0)} rightText={upgrade ? "Upgrade" : "Skip"} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		allowLateCheckout: store.guest.info.guestDetails.stayDetails.allowLateCheckout,
		allowBreakfast: store.guest.info.guestDetails.stayDetails.allowBreakfast,
		confirmationNumber: store.guest.info.confirmationNumber,
		text: store.roomPreferences.text,
		roomTypeCode: store.guest.info.guestDetails.roomDetails.roomTypeCode,
		roomType: store.guest.info.guestDetails.roomDetails.roomType,
		selectedPreferences: store.guest.info.guestDetails.roomDetails.preferences,
		selectedExtraPreferences: store.guest.info.guestDetails.roomDetails.extraPreferences,
		arrivalDate: store.guest.info.guestDetails.stayDetails.arrivalDate,
		departureDate: store.guest.info.guestDetails.stayDetails.departureDate,
	}
})(View);