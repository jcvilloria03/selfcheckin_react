import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			charges: data.charges,
			optional: data.optional,
			required: data.required,
			title: data.title,
			noPreferences: data.no_preferences
		}
	}
}

export default Action;