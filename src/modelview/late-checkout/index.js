import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"

import {
	ButtonImage,
	Footer
} from "../../components"

import {
	timezone
} from "../../env"

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action"

import "./style.css";

class View extends Component {
	page = "pal-late-checkout";

	state = {
		price: 0,
		standardCheckout: "00:00:00",
		lateCheckout: "00:00:00",
	}

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "late_checkout"
			}},
			{url: "interfaceimage", body: {
				name: "late_checkout"
			}},
			{url: "checkoutpage", method: "post", body: {
				confNo: this.props.confirmationNumber
			}}
		];

		const [text, icon, response] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))
		this.props.dispatch(Action.setIcon(icon))

		this.setState({
			price: response.checkout_late_price,
			standardCheckout: response.checkout_standard_time,
			lateCheckout: response.checkout_max_time
		})
	}

	async handleClick(lateCheckout){
		await this.props.dispatch(GuestAction.setLateCheckout(lateCheckout))

		this.props.history.push(this.props.allowBreakfast > 0 ? "/checkin/confirmation" : "/breakfast")
	}


  	render(){
  		const {
  			history,
  			icon,
  			text
  		} = this.props

  		const {
  			price,
  			standardCheckout,
  			lateCheckout
  		} = this.state
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Late Checkout</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<h3 className={"font-normal"}>{`${text.header}`.replace(/\[standard_time\]/g, moment.tz(standardCheckout, "HH:mm:ss", timezone).format("h:mm A")).replace(/\[late_checkout_time\]/g, moment.tz(lateCheckout, "HH:mm:ss", timezone).format("h:mm A"))}</h3>

	    			<div className={"left"}>
		    			<ButtonImage handleClick={this.handleClick.bind(this, 1)} icon={icon.late} />
		    			<p>{text.yes} ({parseFloat(price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')})</p>
		    		</div>

		    		<div className={"right"}>
		    			<ButtonImage handleClick={this.handleClick.bind(this, 0)} icon={icon.standard} />
		    			<p>{text.no}</p>
		    		</div>

		    		<br/>

		    		<p className={"font-italic color-gray align-center instuction"}>{text.footer}</p>
	    		</div>

	    		<Footer renderRight={false} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		allowBreakfast: store.guest.info.guestDetails.stayDetails.allowBreakfast,
		confirmationNumber: store.guest.info.confirmationNumber,
		icon: store.lateCheckout.icon,
		lateCheckout: store.guest.info.guestDetails.stayDetails.lateCheckout,
		text: store.lateCheckout.text
	}
})(View);