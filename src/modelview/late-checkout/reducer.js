import {
	ICON_SET,
	TEXT_SET
} from "./constant";

const initialState = {
	icon: {},
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action);
		case ICON_SET:
			return setIcon(state, action)
		default:
			return state;
	}
}

function setIcon(state, action){
	return {
		...state,
		icon: {
			late: action.late,
			standard: action.standard,
		}
	}
}

function setText(state, action){
	return {
		...state,
		text: {
			title: action.title,
			header: action.header,
			yes: action.yes,
			no: action.no,
			footer: action.footer,
		}
	}
}

export default reducer;