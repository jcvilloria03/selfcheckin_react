import {
	ICON_SET,
	TEXT_SET
} from "./constant";

class Action{
	static setIcon(data){
		return {
			type: ICON_SET,
			late: data.check_out_late,
			standard: data.check_out_standard
		}
	}

	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			header: data.header,
			yes: data.yes,
			no: data.no,
			footer: data.footer
		}
	}
}

export default Action;