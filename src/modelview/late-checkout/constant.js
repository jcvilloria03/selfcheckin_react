const ICON_SET = "late-checkout/ICON_SET"
const TEXT_SET = "late-checkout/TEXT_SET"

export {
	ICON_SET,
	TEXT_SET
}