import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import {
	Footer
} from "../../components"

import Action from "./action"
import Api from "../../api"
import CompanionDetails from "../companion-details/action"
import GuestAction from "../guest/action"
import SignaturePad from 'react-signature-canvas'

import "./style.css";
import signatureClear from "../../assets/img/signature-clear.png"

class View extends Component {
	page = "pal-companion-signature";

	state = {
		error: false
	}

	signature = React.createRef();

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "companion_signature"
			}}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))
	}

	clearSignaturePad(){
		this.signature.current.clear();
	}

	async handleContinueClick(){
		if (this.signature.current.isEmpty()) {
			this.manageError(true)
			return;
		}

		const companion = this.props.companionList[Object.keys(this.props.companionList)[this.props.index]]
		companion.signature = this.signature.current.toDataURL().split("base64,")[1]
		await this.props.dispatch(GuestAction.updateCompanion(companion))
		await this.props.dispatch(CompanionDetails.incrementIndex())
		this.props.history.push((Object.keys(this.props.companionList).length === (this.props.index)) ? "/room/choices" : "/companion/details")
	}

	manageError(error){
		this.setState({
			error
		});
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			error 
  		} = this.state
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Guest Signature</title>
	    		</Header>

	    		<p className={`pal-error${error ? " active" : ""}`} onClick={this.manageError.bind(this, false)}>
    				{ text.error }
    			</p>
    			<br/><br/>
	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<div className={"signature-wrapper"}>
    					<SignaturePad 
	    					ref={this.signature}
                            penColor={"#000"}
				            backgroundColor={'#fff'}
				            marginBottom={'20px'}
				            canvasProps={{width: 500, height: 250}}
                        />
                        <button onClick={this.clearSignaturePad.bind(this)}><img alt={"Clear"} src={signatureClear}/></button>
                        <p className={"font-italic color-gray align-center email-instuction"}>{text.signatureInstruction}</p>
    				</div>
	    		</div>

	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		text: store.companionSignature.text,
		companionList: store.guest.info.companionList,
		index: store.companionDetails.index,
	}
})(View);