import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return{
			type: TEXT_SET,
			title: data.title,
			signatureInstruction: data.signature_instruction,
			error: data.error
		}
	}
}

export default Action;