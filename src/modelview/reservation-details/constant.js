const DETAILS = "reservation-details/DETAILS"
const TEXT_SET = "reservation-details/TEXT_SET"

export {
	DETAILS,
	TEXT_SET
}