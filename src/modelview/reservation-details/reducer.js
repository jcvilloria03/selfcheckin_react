import {
	DETAILS,
	TEXT_SET
} from "./constant";

const initialState = {
	roomTotal: "",
	tax: "",
	grandTotal: "",
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case DETAILS:
			return setDetails(state, action)
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setDetails(state, action){
	return {
		...state,
		roomTotal: action.roomTotal,
		tax: action.tax,
		grandTotal: action.grandTotal
	}
}

function setText(state, action){
	return {
		...state,
		text: {
			title: action.title,
			checkin: action.checkin,
			checkout: action.checkout,
			nights: action.nights,
			guest: action.guest,
			package: action.package,
			roomType: action.roomType,
			total: action.total,
			taxes: action.taxe,
			grandTotal: action.grandTotal,
		}
	}
}

export default reducer;