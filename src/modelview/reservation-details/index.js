import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"

import {
	timezone
} from "../../env"

import {
	Footer,
	Table
} from "../../components"

import Action from "./action"
import Api from "../../api"

import "./style.css";

class View extends Component {
	page = "pal-reservation-details";

	async componentDidMount(){
		if (!this.props.passportNumber && !this.props.rate) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "reservation_details"
			}}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))
	}

	handleContinueClick(){
		this.props.history.push("/guest/details")
	}

  	render(){
  		const {
  			adultCount,
			arrivalDate,
			childCount,
			departureDate,
			noOfNights,
			roomPackage,
			roomType,
  			history,
  			text,
  			roomTotal,
			tax,
			grandTotal
  		} = this.props

	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Reservation Details</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
    				<div className={"details"}>
    					<Table 
		    				head={null}

		    				body={[
			    				(<tr key={"check-in"}>
			    					<td>{text.checkin}: <b className={"float-right"}>{moment.tz(arrivalDate, "YYYY-MM-DD", timezone).format("MMM DD, YYYY")}</b></td>
			    				</tr>),
			    				(<tr key={"check-out"}>
			    					<td>{text.checkout}: <b className={"float-right"}>{moment.tz(departureDate, "YYYY-MM-DD", timezone).format("MMM DD, YYYY")}</b></td>
			    				</tr>),
			    				(<tr key={"no-of-nights"}>
			    					<td>{text.nights}: <b className={"float-right"}>{noOfNights}</b></td>
			    				</tr>),
			    				(<tr key={"no-of-guest"}>
			    					<td>{text.guest}: <b className={"float-right"}>{parseInt(adultCount) + parseInt(childCount)}</b></td>
			    				</tr>),
			    				(<tr key={"package"}>
			    					<td>{text.package}: <b className={"float-right"}>{roomPackage ? roomPackage.toString() : null}</b></td>
			    				</tr>),
			    				(<tr key={"room-type"}>
			    					<td>{text.roomType}: <b className={"float-right"}>{roomType}</b></td>
			    				</tr>),
		    				]}
		    			/>
    				</div>

    				<div className={"pricing"}>
    					<Table 
		    				head={null}

		    				body={[
			    				(<tr key={"room-total"}>
			    					<td className={"align-left"}>{text.total}</td>
			    					<td className={"align-right"}>{roomTotal}</td>
			    				</tr>),
			    				(<tr key={"taxes"}>
			    					<td className={"align-left"}>{text.taxes}</td>
			    					<td className={"align-right"}>{tax}</td>
			    				</tr>),
			    				(<tr className={"total"} key={"grand total"}>
			    					<td className={"align-left color-primary"}><b>{text.grandTotal}</b></td>
			    					<td className={"color-green align-right"}><b>{grandTotal}</b></td>
			    				</tr>),
		    				]}
		    			/>
    				</div>

    				<div>
    					
    				</div>
	    		</div>
	    		
	    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		adultCount: store.guest.info.guestDetails.stayDetails.adultCount,
		arrivalDate: store.guest.info.guestDetails.stayDetails.arrivalDate,
		childCount: store.guest.info.guestDetails.stayDetails.childCount,
		departureDate: store.guest.info.guestDetails.stayDetails.departureDate,
		noOfNights: store.guest.info.guestDetails.stayDetails.noOfNights,
		passportNumber: store.guest.info.guestDetails.passport.number,
		roomPackage: store.guest.info.guestDetails.roomDetails.roomPackage,
		roomType: store.guest.info.guestDetails.roomDetails.roomType,
		text: store.reservationDetails.text,
		roomTotal: store.reservationDetails.roomTotal,
		tax: store.reservationDetails.tax,
		grandTotal: store.reservationDetails.grandTotal,
	}
})(View);