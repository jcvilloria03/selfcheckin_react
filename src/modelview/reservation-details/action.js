import {
	DETAILS,
	TEXT_SET
} from "./constant";

class Action{
	static setDetails(data){
		return {
			type: DETAILS,
			roomTotal: data.roomTotal,
			tax: data.tax,
			grandTotal: data.total
		}
	}

	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			checkin: data.checkin,
			checkout: data.checkout,
			nights: data.nights,
			guest: data.guests,
			package: data.package,
			roomType: data.room_type,
			total: data.total,
			taxe: data.taxes,
			grandTotal: data.grand_total
		}
	}
}

export default Action;