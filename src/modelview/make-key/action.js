import {
	ICON_SET,
	TEXT_SET
} from "./constant";

class Action{
	static setIcon(data){
		return {
			type: ICON_SET,
			image: data.image
		}
	}

	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			header: data.header,
			instruction: data.instruction
		}
	}
}

export default Action;