import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import io from "socket.io-client"
import moment from "moment-timezone"

import {
	websocket,
	timezone
} from "../../env"

import {
	Footer
} from "../../components"

import Action from "./action"
import Api from "../../api"
import Loader from 'react-loader-spinner'

import "./style.css";

class View extends Component {
	page = "make-key";

	socket = io(`${websocket}/`, {
		transports: ['websocket'], 
		upgrade: false,
		query: `macAddress=${localStorage.getItem('macAddress')}`
	});

	state = {
		error: {
			status: false,
			message: null
		},
		loader: false
	}

	async componentDidMount(){
		if (!this.props.guest.reservationId) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "make_key"
			}},
			{url: "interfaceimage", body: {
				name: "make_key"
			}}
		];

		const [text, icon] = await Api.fetch(requests);

		this.props.dispatch(Action.setText(text))
		this.props.dispatch(Action.setIcon(icon))

		this.socket.on("make key response", this.handleMakeKeyResponse.bind(this))
	}

	async handleContinueClick(){
		const {
			guest
		} = this.props;

		this.setState({
			loader: true
		})

		this.socket.emit("make key", {
			checkin: moment.tz(guest.guestDetails.stayDetails.arrivalDate, "YYYY-MM-DD", timezone).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
			checkout: moment.tz(guest.guestDetails.stayDetails.departureDate, "YYYY-MM-DD", timezone).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
			guestName: `${guest.guestDetails.firstname} ${guest.guestDetails.lastname}`,
			roomNumber: String(guest.guestDetails.roomDetails.roomNumber),
			keyCount: 1,
			reservationId: guest.reservationId,
			pmsId: "QPADEV0029"
		})
	}

	handleError(error, message){
		this.setState({
			loader: false,
			error: {
				status: error,
				message
			}
		})
	}

	handleMakeKeyResponse(data){
		if (data.StatusCode === "0") {
			this.props.history.push("checkin/success")
			return;
		}

		this.setState({
			loader: false,
			error: {
				status: data.StatusCode !== 0,
				message: data.ErrorMessage
			}
		})
	}

  	render(){
  		const {
  			history,
  			icon,
  			text
  		} = this.props

  		const {
  			loader,
  			error
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Front Page</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<p className={`pal-error${error.status ? " active" : ""}`} onClick={this.handleError.bind(this, false, null)}>
	    				{ error.message }
	    			</p>
	    			<br/>
	    			<br/>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
	    			<h3 className={"font-normal"}>{text.header}</h3>

	    			<img alt={"key"} src={icon.image}/>
	    			<p className={"font-italic color-gray align-center instuction"}>{text.instruction}</p>
	    		</div>

	    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)} rightText={"Make Key"}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		guest: store.guest.info,
		icon: store.makeKey.icon,
		text: store.makeKey.text
	}
})(View);