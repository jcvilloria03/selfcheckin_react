import {
	ICON_SET,
	TEXT_SET	
} from "./constant";

const initialState = {
	icon: {},
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case ICON_SET:
			return setIcon(state, action)
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return{
		...state,
		text: {
			title: action.title,
			header: action.header,
			instruction: action.instruction
		}
	}
}	

function setIcon(state, action){
	return{
		...state,
		icon: {
			image: action.image
		}
	}
}

export default reducer;