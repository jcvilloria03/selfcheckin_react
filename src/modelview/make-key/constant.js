const ICON_SET = "make-key/ICON_SET"
const TEXT_SET = "make-key/TEXT_SET"

export {
	ICON_SET,
	TEXT_SET
}