import {
	SET_TEXT_UI
} from "./constant";

class Action{
	static setText(data){
		return {
			type: SET_TEXT_UI,
			confirmationPlaceholder: data.confirmation_placeholder,
			firstnamePlaceholder: data.firstname_placeholder,
			lastnamePlaceholder: data.lastname_placeholder,
			or: data.or,
			title: data.title,
			error: data.error
		};
	}
}

export default Action;