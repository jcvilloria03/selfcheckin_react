import {
	SET_TEXT_UI
} from "./constant";

const initialState = {
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case SET_TEXT_UI:
			return setTextUI(state, action);
		default:
			return state;
	}
}

function setTextUI(state, action){
	return {
		...state,
		text: {
			confirmationPlaceholder: action.confirmationPlaceholder,
			firstnamePlaceholder: action.firstnamePlaceholder,
			lastnamePlaceholder: action.lastnamePlaceholder,
			or: action.or,
			title: action.title,
			error: action.error
		}
	}
}

export default reducer;