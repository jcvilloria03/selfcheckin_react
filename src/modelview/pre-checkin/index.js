import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import Loader from 'react-loader-spinner'

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action";
import ReservationDetailsAction from "../reservation-details/action"

import {
	Footer,
	Textbox
} from "../../components"

import "./style.css";

class View extends Component {
	page = "pal-pre-checkin";

	state = {
		error: false,
		loader: false,
		firstname: this.props.firstname,
		lastname: this.props.lastname
	}

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{url: "interface", body: {
				name: "pre_checkin"
			}}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text));
	}

	handleContinueClick(){
		this.setState({loader: true});

		setTimeout(async ()=>{
			let request;

			if (this.state.firstname !== "" && this.state.lastname !== "" && this.state.firstname && this.state.lastname) {
				request = [
					{
						url: "booking", 
						method: "post",
						body: {
							firstName: this.state.firstname,
							lastName: this.state.lastname
						}
					}
				];
			}

			if (this.state.confNo !== "" && this.state.confNo) {
				request = [
					{
						url: "booking", 
						method: "post",
						body: {
							confNo: this.state.confNo
						}
					}
				];
			}

			if (!request){
				this.setState({loader: false});
				return;
			}

			const [guest] = await Api.fetch(request);

			if (!guest.status){
				this.manageError(true);
				return;
			}

			this.setState({loader: false});
				
			this.props.dispatch(GuestAction.setGuestInfo(guest));
			this.props.dispatch(ReservationDetailsAction.setDetails({
				roomTotal: guest.rates,
				tax: guest.taxes,
				total: guest.total
			}))
			this.props.history.push(guest.earlyCheckin ? "/early-checkin" : "reservation-details")
		}, 300);
	}

	handleTextChange(e){
		const input = e.currentTarget;

		this.setState({ [input.getAttribute("name")]: input.value });
	}

	manageError(error){
		this.setState({
			loader: false,
			error
		});
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			error,
  			loader,
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={`${this.page}`}>
	    		<Header key={this.page}>
	    			<title>Pre Checkin</title>
	    		</Header>

	    		<p className={`pal-error${error ? " active" : ""}`} onClick={this.manageError.bind(this, false, null, null)}>
    				{ text.error }
    			</p>
    			<br/><br/>
	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

		    		<Textbox type={"text"} handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)} name={"confNo"} numberOnly={true} className={"align-center"} placeholder={text.confirmationPlaceholder}/>
		    		<h2 className={"font-normal color-primary"}>{text.or}</h2>
		    		<Textbox handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)} value={this.state.firstname} name={"firstname"} type="text" className={"align-center"} placeholder={text.firstnamePlaceholder}/>
		    		<Textbox handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)} value={this.state.lastname} name={"lastname"} type="text" className={"align-center"} placeholder={text.lastnamePlaceholder}/>
	    		</div>

	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		firstname: store.guest.info.guestDetails.firstname,
		lastname: store.guest.info.guestDetails.lastname,
		text: store.preCheckin.text
	}
})(View);