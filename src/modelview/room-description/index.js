import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import { Carousel } from 'react-responsive-carousel';

import {
	Footer
} from "../../components"

import Api from "../../api"
import GuestAction from "../guest/action"

import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./style.css";

class View extends Component {
	page = "pal-room-description";

	state = {
		skip: false
	}

	async componentDidMount(){
		if (!this.props.code && !this.props.passportNumber) {
			this.props.history.push("/room/choices");
			return;
		}

		let requests = [
			{
				url: "resvAdvanced/fetchRoomStatus",
				method: "post",
				body: {
					confNo: this.props.confirmationNumber,
					room_type: this.props.code
				}
			}
		];

		const [upgrade] = await Api.fetch(requests);
		this.setState({
			skip: this.props.selected || !upgrade.status
		});

		this.props.dispatch(GuestAction.unsetRoomUpgrade());
	}

	async handleContinueClick(){
		if (this.state.skip) {
			this.props.history.push(this.props.allowLateCheckout === "1" ? "/late-checkout" : 
				this.props.allowBreakfast > 0 ? "/checkin/confirmation" : "/breakfast")
			return;
		}

		let requests = [
			{
				url: "resvAdvanced/fetchRoomStatus",
				method: "post",
				body: {
					confNo: this.props.confirmationNumber,
					room_type: this.props.code,
					upgrade: 1
				}
			}
		];

		const [upgrade] = await Api.fetch(requests);
		upgrade["name"] = this.props.name
		await this.props.dispatch(GuestAction.upgradeRoom(upgrade))

		this.props.history.push("/room/preferences")
	}

  	render(){
  		const {
  			history,
  			name,
			images,
			description,
			price
  		} = this.props

  		const {
  			skip
  		} = this.state
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Room Description</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{name}</h1>
	    			<br/>
	    			<Carousel emulateTouch showThumbs={true} 
		    			showStatus={false} showArrows={false} infiniteLoop autoPlay interval={7000}>
		    			{images.map((data, index)=>(
		    				<div key={`${name} ${index}`}>
								<img alt={`${name} ${index}`} src={data} />
							</div>
		    			))}
					</Carousel>

					<div className={"details align-left"}>
		    			<p>Price: <b>{parseFloat(price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</b></p>
		    			<p>Description:</p>
		    		</div>

		    		<div className={"align-justify"} dangerouslySetInnerHTML={{__html: description}} />
	    		</div>

	    		<Footer history={history} rightText={!skip ? "Upgrade" : "Skip"} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		allowLateCheckout: store.guest.info.guestDetails.stayDetails.allowLateCheckout,
		allowBreakfast: store.guest.info.guestDetails.stayDetails.allowBreakfast,
		passportNumber: store.guest.info.guestDetails.passport.number,
		confirmationNumber: store.guest.info.confirmationNumber,
		code: store.roomDescription.code,
		name: store.roomDescription.name,
		images: store.roomDescription.images,
		description: store.roomDescription.description,
		price: store.roomDescription.price,
		selected: store.roomDescription.selected,
		roomTypeOld: store.guest.info.guestDetails.roomDetails.roomTypePrev,
	}
})(View);