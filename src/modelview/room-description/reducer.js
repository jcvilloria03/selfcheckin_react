import {
	ROOM_DETAILS_SET
} from "./constant";

const initialState = {
	name: "",
	images: [""]
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case ROOM_DETAILS_SET:
			return setRoomDetails(state, action)
		default:
			return state;
	}
}

function setRoomDetails(state, action){
	return {
		...state,
		code: action.code,
		name: action.name,
		images: action.images,
		description: action.description,
		price: action.price,
		selected: action.selected,
	}
}

export default reducer;