import {
	ROOM_DETAILS_SET
} from "./constant";

class Action{
	static setRoomDetails(data){
		return {
			type: ROOM_DETAILS_SET,
			...data
		}
	}
}

export default Action;