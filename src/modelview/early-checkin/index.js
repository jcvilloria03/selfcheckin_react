import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"

import {
	ButtonImage,
	Footer
} from "../../components"

import {
	timezone
} from "../../env"

import Action from "./action"
import Api from "../../api"
import GuestAction from "../guest/action"

import "./style.css";

class View extends Component {
	page = "pal-early-cheackin";

	state = {
		checklater: false,
		time: "00:00:00",
		rate: null
	}

	async componentDidMount(){
		if (!this.props.confirmationNumber && !this.props.rate && !this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}
		
		let requests = [
			{url: "interface", body: {
				name: "early_checkin"
			}},
			{url: "interfaceimage", body: {
				name: "early_checkout"
			}},
			{url: "checkinpage", method: "post", body: {
				confNo: this.props.confirmationNumber
			}}
		];

		const [text, icon, earlyResponse] = await Api.fetch(requests);
		
		this.props.dispatch(Action.setText(text))
		this.props.dispatch(Action.setIcon(icon))

		this.setState({
			time: earlyResponse.checkin_standard_time,
			rate: earlyResponse.checkin_early_rate
		})
	}

	handleCheckInClick(){
		this.props.dispatch(GuestAction.setEarlyCheckin(1))
		this.props.history.push("/reservation-details")
	}

	handleCheckLaterContinueClick(){
		window.location.href= "/"
	}

	manageCheckLaterStatus(checklater){
		this.setState({ checklater });
	}

  	render(){
  		const {
  			history,
  			icon,
  			text
  		} = this.props

  		const {
  			checklater,
  			time,
			rate
  		} = this.state

  		if (checklater)
  		return (
  			<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Early Checkin</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.checklaterTitle}</h1>
					<h3 className={"font-normal"}>{text.checklaterHead}</h3>
	    		</div>

	    		<Footer handleBackClick={this.manageCheckLaterStatus.bind(this, false)} handleContinueClick={this.handleCheckLaterContinueClick.bind(this)} history={history}/>
	    	</div>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Early Checkin</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
	    			<h3 className={"font-normal"}>{`${text.header}`.replace(/\[time\]/g, moment.tz(time, "HH:mm:ss", timezone).format("h:mm A"))}</h3>
	    			<div className={"check-in"}>
		    			<ButtonImage icon={icon.now} handleClick={this.handleCheckInClick.bind(this)}/>
		    			<p>{text.checkin} {parseFloat(rate) > 0 ? `(${parseFloat(rate).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')})` : null}</p>
		    		</div>

		    		<div className={"check-out"}>
		    			<ButtonImage icon={icon.later} handleClick={this.manageCheckLaterStatus.bind(this, true)}/>
		    			<p>{text.checklater}</p>
		    		</div>
	    		</div>

	    		<Footer renderRight={false} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		confirmationNumber: store.guest.info.confirmationNumber,
		icon: store.earlyCheckin.icon,
		text: store.earlyCheckin.text
	}
})(View);