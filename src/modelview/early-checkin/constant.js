const ICON_SET = "early-checkin/ICON_SET"
const TEXT_SET = "early-checkin/TEXT_SET"

export {
	ICON_SET,
	TEXT_SET
}