import {
	ICON_SET,
	TEXT_SET
} from "./constant";

class Action{
	static setIcon(data){
		return {
			type: ICON_SET,
			now: data.check_in_now,
			later: data.check_in_later
		}
	}

	static setText(data){
		return{
			type: TEXT_SET,
			title: data.title,
			header: data.header,
			checkin: data.checkin_label,
			checklater: data.checklater_label,
			checklaterTitle: data.checklater_title,
			checklaterHead: data.checklater_head
		}
	}
}

export default Action;