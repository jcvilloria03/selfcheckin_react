import React, { Component } from 'react';
import { connect } from "react-redux";

import {
	Footer
} from "../../components"

import "./style.css";

class View extends Component {
	page = "pal-facial-verification";


  	render(){
  		const {
  			history
  		} = this.props
  		
	    return(
	    	<div className={this.page}>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>VERIFICATION</h1>

	    			<div>
	    				<div className={"facial-wrapper"}>
		    				<div className={"image-wrapper"}></div>
		    				<p>Passport Photo</p>
		    			</div>

		    			<div className={"facial-wrapper"}>
		    				<div className={"image-wrapper"}></div>
		    				<label htmlFor="captureGuest" className="pal-button">
					        	Capture
					        	<input id="captureGuest" type="file" name="captureGuest" onChange={null}/>
					        </label>
		    			</div>
	    			</div>
	    		</div>

	    		<Footer history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {

	}
})(View);