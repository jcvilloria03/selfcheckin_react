import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";

import {
	Checkbox,
	Footer,
	Textbox
} from "../../components"

import Action from "./action"
import Api from "../../api"
import Loader from 'react-loader-spinner'
import SignaturePad from 'react-signature-canvas'
import Validate from "../../validation"

import "./style.css";
import signatureClear from "../../assets/img/signature-clear.png"

class View extends Component {
	page = "pal-checkout-confirmation";

	signature = React.createRef();

	state = {
		eBill: 1,
		error: false,
		errorMessage: null,
		loader: false,
	}

	componentWillMount(){
		if (!this.props.confirmationNumber && !this.props.reservationID) {
			this.props.history.push("/pre-checkout");
			return;
		}
	}

	async componentDidMount(){
		let requests = [
			{
				url: "interface",
				body: {
					name: "checkout_confirmation"
				}
			}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text))
	}

	clearSignaturePad(){
		this.signature.current.clear();
	}

	handleContinueClick(){
		const signature = this.signature.current;

		this.setState({loader: true});

		setTimeout(async ()=>{
			let {
				eBill,
				email
			} = this.state

			email = email ? email : this.props.email;

			if (signature.isEmpty()) {
				this.manageError(true, this.props.text.signatureError)
				return;
			}

			if (eBill === 1 && !Validate.email(email)) {
				this.manageError(true, this.props.text.emailError)
				return;
			}

			let requests = [
				{
					url: "checkout",
					method: "post",
					body: {
						confNo: this.props.confirmationNumber,
						base64Data: signature.toDataURL().split("base64,")[1],
						resvId: this.props.reservationID,
						profileId: this.props.profileID,
						email: email,
						eBill: eBill
					}
				}
			];

			const [checkout] = await Api.fetch(requests);

			if (checkout.status === false) {
				this.manageError(true, this.props.text.error)
				return;
			}

			this.props.history.push("/checkout/success");
		}, 300);
	}

	handleEBillChange(eBill){
		this.setState({eBill});
	}

	handleTextChange(e){
		const input = e.currentTarget;

		this.setState({ [input.getAttribute("name")]: input.value });
	}

	manageError(error, errorMessage){
		this.setState({
			error,
			errorMessage: errorMessage,
			loader: false
		});
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			eBill,
  			error,
  			errorMessage,
  			loader
  		} = this.state

  		if (loader)
  		return(
  			<Loader type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Checkout Confirmation</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<p className={`pal-error${error ? " active" : ""}`} onClick={this.manageError.bind(this, false, null, null)}>
	    				{ errorMessage }
	    			</p>

	    			<div className={"align-left"}>
	    				<h2 className={"font-normal"}>{text.ebillTitle}</h2>

	    				<Checkbox checked={eBill === 1} handleChange={this.handleEBillChange.bind(this, 1)}>{text.ebillYes}</Checkbox>
	    				<Textbox type="text" className={"align-center"} placeholder={text.emailPlaceholder} name={"email"} handleEnterPress={this.handleContinueClick.bind(this)} handleChange={this.handleTextChange.bind(this)}/>
	    				<p className={"font-italic color-gray align-center email-instuction"}>{text.emailInstruction}</p>
	    				<Checkbox checked={eBill === 2} handleChange={this.handleEBillChange.bind(this, 2)}>{text.ebillNo}</Checkbox>

	    				<div className={"signature-wrapper"}>
	    					<SignaturePad 
		    					ref={this.signature}
	                            penColor={"#000"}
					            backgroundColor={'#fff'}
					            marginBottom={'20px'}
					            canvasProps={{width: 500, height: 250}}
	                        />
	                        <button onClick={this.clearSignaturePad.bind(this)}><img alt={"Clear"} src={signatureClear}/></button>
	                        <p className={"font-italic color-gray align-center email-instuction"}>{text.signatureInstruction}</p>
	    				</div>
	    			</div>
	    		</div>

	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		confirmationNumber: store.guest.info.confirmationNumber,
		email: store.guest.info.guestDetails.email,
		profileID: store.guest.info.guestDetails.profileId,
		reservationID: store.guest.info.reservationId,
		text: store.checkoutConfirmation.text
	}
})(View);