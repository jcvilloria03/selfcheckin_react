import {
	TEXT_SET
} from "./constant";

const initialState = {
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return {
		...state,
		text: {
			title: action.title,
			ebillTitle: action.ebillTitle,
			ebillYes: action.ebillYes,
			emailInstruction: action.emailInstruction,
			emailPlaceholder: action.emailPlaceholder,
			ebillNo: action.ebillNo,
			signatureInstruction: action.signatureInstruction,
			signatureError: action.signatureError,
			error: action.error,
			emailError: action.emailError
		}
	}
}

export default reducer;