import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			ebillTitle: data.ebill_title,
			ebillYes: data.ebill_yes,
			emailInstruction: data.email_instruction,
			emailPlaceholder: data.email_placeholder,
			ebillNo: data.ebill_no,
			signatureInstruction: data.signature_instruction,
			signatureError: data.signature_error,
			error: data.error,
			emailError: data.email_error
		}
	}
}

export default Action;