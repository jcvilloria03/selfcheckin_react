import {
	TEXT_SET
} from "./constant";

const initialState = {
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return {
		...state,
		text: {
			title: action.title,
			header: action.header,
			cardType: action.cardType,
			name: action.name,
			expiration: action.expiration,
			newCard: action.newCard,
			number: action.number
		}
	}
}

export default reducer;