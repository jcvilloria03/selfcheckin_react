import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"

import {
	Button,
	Footer
} from "../../components"

import {
	timezone
} from "../../env"

import Action from "./action"
import Api from "../../api"

import "./style.css";

class View extends Component {
	page = "pal-card-details";

	state = {

	}

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		let requests = [
			{
				url: "interface",
				body: {
					name: "card_details"
				}
			},
			{
				url: "name/FetchCreditCardList", 
				method: "post",
				body: {
					profileid: this.props.profileID,
					primary: 1
				}
			}
		];

		const [text, card] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text));

		if (!card.cardcode) return;

		this.setState({
			cardType: card.cardcode,
			cardName: card.cardholdername,
			cardNumber: card.cardnumber,
			expirationDate: moment.tz(card.expirationdate, "YYYY-MM-DD", timezone).format("MM/YY")
		});
	}

	handleContinueClick(){
		if (Object.keys(this.props.companionList).length > 0)
			this.props.history.push("/companion/details")
		else
			this.props.history.push("/room/choices")
	}

	handleNewCardClick(){
		this.props.history.push("/card/new")
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			cardType,
			cardName,
			cardNumber,
			expirationDate,
  		} = this.state
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Card Details</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
	    			<h3 className={"font-normal"}>{text.header}</h3>
	    			<div className={"details align-left"}>
		    			<p>{text.cardType}: <b>{cardType}</b></p>
		    			<p>{text.name}: <b>{cardName}</b></p>
		    			<p>{text.number}: <b>{cardNumber}</b></p>
		    			<p>{text.expiration}: <b>{expirationDate}</b></p>
		    		</div>
		    		<Button handleClick={this.handleNewCardClick.bind(this)}>{text.newCard}</Button>
	    		</div>

	    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		companionList: store.guest.info.companionList,
		passportNumber: store.guest.info.guestDetails.passport.number,
		profileID: store.guest.info.guestDetails.profileId,
		text: store.cardDetails.text
	}
})(View);