import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			header: data.header,
			cardType: data.type,
			name: data.name,
			expiration: data.expiration,
			newCard: data.new_card,
			number: data.number
		}
	}
}

export default Action;