import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			name: data.name_label,
			noOfNights: data.no_of_nights_label,
			date: data.date_label,
			description: data.description_label,
			amount: data.amount_label,
			subtotal: data.subtotal_label,
			deposit: data.deposit_label,
			balanceDue: data.balance_due_label
		}
	}
}

export default Action;