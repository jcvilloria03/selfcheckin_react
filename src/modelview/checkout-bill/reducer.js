import {
	TEXT_SET
} from "./constant";

const initialState = {
	text: {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case TEXT_SET:
			return setText(state, action)
		default:
			return state;
	}
}

function setText(state, action){
	return {
		...state,
		text: {
			title: action.title,
			name: action.name,
			noOfNights: action.noOfNights,
			date: action.date,
			description: action.description,
			amount: action.amount,
			subtotal: action.subtotal,
			deposit: action.deposit,
			balanceDue: action.balanceDue
		}
	}
}

export default reducer;