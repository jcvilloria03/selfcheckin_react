import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"

import Action from "./action"
import Api from "../../api"
import Loader from 'react-loader-spinner'

import {
	Footer,
	Table
} from "../../components"

import {
	timezone
} from "../../env"

import "./style.css";

class View extends Component {
	page = "pal-checkout-bill";

	state = {
		billList: [],
		balanceDue: null,
		deposit: null,
		subTotal: null,
		loader: true,
	}

	componentWillMount(){
		if (!this.props.confirmationNumber && !this.props.reservationID) {
			this.props.history.push("/pre-checkout");
			return;
		}
	}

	async componentDidMount(){
		let requests = [
			{
				url: "interface",
				body: {
					name: "checkout_bill"
				}
			}
		];

		const [text] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text));

		requests = [
			{
				url: "invoice", 
				method: "post",
				body: {
					confNo: this.props.confirmationNumber
				}
			}
		];
		const [bills] = await Api.fetch(requests);

		if (!bills.status) return;

		this.setState({
			billList: bills.billItems,
			subTotal: bills.subTotal,
			deposit: bills.deposit,
			balanceDue: bills.balanceDue,
			loader: false
		});
	}

	handleContinueClick(){
		setTimeout(()=>{
			this.props.history.push("/checkout/confirmation");
		}, 300);
	}

  	render(){
  		const {
  			firstname,
  			history,
  			lastname,
  			nights,
  			text
  		} = this.props

  		const {
  			billList,
			balanceDue,
			deposit,
  			subTotal,
  			loader
  		} = this.state

	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>Checkout Bill</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>
		    		<div className={"details align-left"}>
		    			<p>{text.name}: <b>{firstname} {lastname}</b></p>
		    			<p>{text.noOfNights}: <b>{nights}</b></p>
		    		</div>

		    		<div className={"scroller"}>
		    			<Table 
		    				head={(
		    					<tr>
			    					<th>{text.date}</th>
			    					<th>{text.description}</th>
			    					<th>{text.amount}</th>
			    				</tr>
		    				)}

		    				body={loader ? (
		    					<tr>
									<td colSpan={3}><Loader type={"ThreeDots"}/></td>
								</tr>
		    				) : (billList.map((data, i)=>(
								<tr key={`bill ${i}`}>
									<td>{moment.tz(data.datePosted, timezone).format("MMMM DD, YYYY")}</td>
									<td className={"align-left"}>{data.description}</td>
									<td className={"align-right"}>{data.amount} {data.currencyCode}</td>
								</tr>
							)))}
		    			/>
		    		</div>
		    		

		    		<div className={"details align-left"}>
		    			<p>{text.subtotal} <b className={"float-right color-green"}>{subTotal}</b></p>
		    			<p>{text.deposit} <b className={"float-right color-green"}>{deposit}</b></p>
		    			<p>{text.balanceDue} <b className={`float-right ${parseFloat(balanceDue) < 0 ? "color-red" : "color-green"}`}>{balanceDue}</b></p>
		    		</div>
	    		</div>
	    		
	    		<Footer handleContinueClick={this.handleContinueClick.bind(this)} history={history}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		confirmationNumber: store.guest.info.confirmationNumber,
		firstname: store.guest.info.guestDetails.firstname,
		lastname: store.guest.info.guestDetails.lastname,
		nights: store.guest.info.guestDetails.stayDetails.noOfNights,
		text: store.checkoutBill.text,
	}
})(View);