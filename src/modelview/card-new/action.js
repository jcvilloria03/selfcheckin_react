import {
	TEXT_SET
} from "./constant";

class Action{
	static setText(data){
		return {
			type: TEXT_SET,
			title: data.title,
			cardType: data.card_type,
			cardName: data.card_name,
			cardNumber: data.card_number,
			expiration: data.expiration,
			errorRequired: data.error_required,
		}
	}
}

export default Action;