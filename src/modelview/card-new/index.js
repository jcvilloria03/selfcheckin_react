import React, { Component } from 'react';
import { connect } from "react-redux";
import {Helmet as Header} from "react-helmet";
import moment from "moment-timezone"
import cardValidator from "card-validator"

import {
	DatePicker,
	Dropdown,
	Footer,
	Textbox
} from "../../components"

import {
	timezone
} from "../../env"

import Action from "./action"
import Api from "../../api"
import Loader from 'react-loader-spinner'
import Validate from "../../validation"

import "./style.css";

class View extends Component {
	page = "pal-card-new";

	state = {
		creditCardType: [],
		creditCardDiscover: {},
		error: {
			status: false,
			message: ""
		},
		formData: {
			cCardHolder: "",
			cCardNumber: "",
			cCardType: "",
			expiryDate: ""
		},
		cardType: null,
		loader: false
	}

	async componentDidMount(){
		if (!this.props.passportNumber) {
			this.props.history.push("/passport-scan");
			return;
		}

		document.addEventListener("keydown", this.handleCardSwiped.bind(this))

		let requests = [
			{
				url: "interface",
				body: {
					name: "card_new"
				}
			},
			{
				url: "creditcardtypes"
			}
		];

		const [text, creditCardType] = await Api.fetch(requests);
		this.props.dispatch(Action.setText(text));

		const cardTypeList = creditCardType.map(card=>{
			return {
				value: card.code,
				label: card.name
			}
		})

		const creditCardDiscover = {}

		for(let i = 0; i < creditCardType.length; i++){
			creditCardDiscover[creditCardType[i].name] = {
				value: creditCardType[i].code,
				label: creditCardType[i].name
			}
		}

		this.setState({
			creditCardType: cardTypeList,
			creditCardDiscover
		})
	}

	async handleContinueClick(){
		const formData = {
			...this.state.formData,
		};

		if (!formData.cCardType && !formData.expiryDate) {
			this.handleError(true, this.props.text.errorRequired)
			return
		}

		formData.cCardType = this.state.cardType.value;

		let required = Object.keys(formData).map(key=>{
			return {
				key: key,
				value: formData[key]
			}
		})

		required = Validate.required(required)

		if (required.length !== 0) {
			this.setErrorInput(required)
			this.handleError(true, this.props.text.errorRequired)
			return;
		}

		formData.profileid = this.props.profileID;

		this.setState({ loader: true });

		let requests = [
			{
				url: "name/InsertCreditCard",
				method: "POST",
				body: formData
			}
		];

		const [response] = await Api.fetch(requests);
		
		if (!response.status) {
			this.handleError(true, response.message)
			return;
		}

		this.props.history.goBack()
	}

	handleCardSwiped(e){
		if (e.key === "%") {
			document.querySelector(".text-swipe").focus()
		}
	}

	handleCardTypeChange(selected){
		this.setState({
			cardType: selected
		});
	}

	handleError(error, message){
		this.setState({ 
			error: {
				status: error,
				message
			},
			loader: false
		});
	}

	setErrorInput(data){
		for(let i = 0; i < data.length; i++){
			const element = document.querySelector(`[name="${data[i]}"]`)
			element.className += " error"
		}
	}

	handleExpirationDateChange(date){
		this.setState({
			formData: {
				...this.state.formData,
				expiryDate: moment.tz(date, timezone).format("YYYY-MM-DD")
			}
		});
	}

	handleTextChange(e){
		const input = e.currentTarget;

		this.setState({ 
			formData: {
				...this.state.formData,
				[input.getAttribute("name")]: input.value
			}
		});
	}

	handleTextSwipeChange(e){
		const text = e.currentTarget.value

		if (text.substring(0,2) === "%B" && e.keyCode === 13) {
			const cardNumber = text.split("^").length > 0 ? text.split("^")[0].substring(2): ""
			const expiryDate = text.split("=").length > 1 ? moment.tz(text.split("=")[1].substring(0, 4), "YYMM", timezone).format("YYYY-MM-DD") : null
			const card = cardValidator.number(cardNumber)

			if (card.isValid) {
				document.querySelector(`[name="cCardNumber"]`).value = cardNumber
				this.setState({
					cardType: this.state.creditCardDiscover[card.card.niceType],
					formData: {
						...this.state.formData,
						cCardNumber: cardNumber,
						expiryDate
					}
				})
			}

			e.currentTarget.value = ""
		}
	}

  	render(){
  		const {
  			history,
  			text
  		} = this.props

  		const {
  			creditCardType,
  			error,
  			formData,
  			cardType,
  			loader
  		} = this.state

  		if (loader)
  		return(
  			<Loader className={"asd"} type={"ThreeDots"}/>
  		);
  		
	    return(
	    	<div className={this.page}>
	    		<Header key={this.page}>
	    			<title>New Credit Card</title>
	    		</Header>

	    		<div className={"animate-wrapper"}>
	    			<p className={`pal-error${error.status ? " active" : ""}`} onClick={this.handleError.bind(this, false, "")}>
	    				{ error.message }
	    			</p>

	    			<h1 className={"font-normal color-primary"}>{text.title}</h1>

	    			<input type={"text"} className={"text-swipe"} onKeyDown={this.handleTextSwipeChange.bind(this)}/>

	    			<Dropdown className={"drop-down"} placeholder={text.cardType} options={creditCardType} value={cardType} handleChange={this.handleCardTypeChange.bind(this)}/>
	    			<Textbox type="text" className={"align-center"} value={formData.cCardHolder} placeholder={text.cardName} name={"cCardHolder"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    			<Textbox type="text" className={"align-center"} value={formData.cCardNumber} placeholder={text.cardNumber} name={"cCardNumber"} handleChange={this.handleTextChange.bind(this)} handleEnterPress={this.handleContinueClick.bind(this)}/>
	    			<DatePicker format={"MM/YY"} minDate={new Date()} placeholder={text.expiration} selected={formData.expiryDate ? new Date(formData.expiryDate) : null} handleChange={this.handleExpirationDateChange.bind(this)}/>
	    		</div>

	    		<Footer history={history} handleContinueClick={this.handleContinueClick.bind(this)}/>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		passportNumber: store.guest.info.guestDetails.passport.number,
		profileID: store.guest.profileID,
		text: store.cardNew.text
	}
})(View);