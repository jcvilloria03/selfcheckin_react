class Validation{
	static email(email){
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(String(email).toLowerCase());
	}

	static required(data){
		const error = []

		for(let i = 0; i < data.length; i++){
			if (data[i].value.trim() === "") {
				error.push(data[i].key)
			}
		}

		return error
	}
}

export default Validation;