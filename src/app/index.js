import React, { Component } from 'react';
import { connect } from "react-redux";
import { Router } from "react-router-dom";
import createBrowserHistory from 'history/createBrowserHistory';
import {Helmet as Header} from "react-helmet";
import { Carousel } from 'react-responsive-carousel';

import {
	CancelButton,
	HelpButton
} from "../components"

import Action from "./action"
import Api from "../api"
import Route from "./route"
import Welcome from "../modelview/welcome"
import Cookies from 'universal-cookie';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./style.css";

const cookies = new Cookies();
const history = createBrowserHistory();

class View extends Component {
	state = {
		render: 0,
		promotion: {
			images: [],
			interval: 10000
		}
	}

	componentWillMount(){

		if (history.location.pathname === "/"){
			cookies.remove("language");
			return
		}

		this.setState({render: 1});
	}

	async componentDidMount(){
		let requests = [
			{url: "theme"},
			{url: "banner", body: {
				encrypted: true
			}},
			{url: "banner-interval", body: {
				encrypted: true
			}}
		];

		let [theme, banner, interval] = await Api.fetch(requests);

		banner = banner.map(data=>(data.images.map(img=>(img.path))))
		banner = [].concat.apply([], banner)

		this.props.dispatch(Action.setTheme(theme))
		this.setState({
			promotion: {
				images: banner,
				interval: interval ? (interval.device_duration ? (interval.device_duration * 1000) : 10000) : 10000
			}
		})
	}

	handleRenderApp(){
		this.setState({render: 1});
	}

  	render(){
  		const {
  			background,
  			logo
  		} = this.props;

  		const renderDynamicStyle = ()=>(
  			<Header key={"app"}>
	    		<style>{`
	    			input[type="text"]:focus, input[type="number"]:focus{
						border-bottom: 2px solid #3aafe6;
					}

					table th{
						background: #3aafe6
					}

	    			.color-primary{
	    				color: #3aafe6;
	    			}

	    			.pal-content{
	    				background-image: url(${background.image});
	    			}

	    			.pal-button{
	    				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#60b0d6+0,216798+100 */
						background: #60b0d6; /* Old browsers */
						background: -moz-linear-gradient(top, #60b0d6 0%, #216798 100%); /* FF3.6-15 */
						background: -webkit-linear-gradient(top, #60b0d6 0%,#216798 100%); /* Chrome10-25,Safari5.1-6 */
						background: linear-gradient(to bottom, #60b0d6 0%,#216798 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
						filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#60b0d6', endColorstr='#216798',GradientType=0 ); /* IE6-9 */

						-webkit-box-shadow: 0px 2px 0px 1px rgba(39,98,158,1);
						-moz-box-shadow: 0px 2px 0px 1px rgba(39,98,158,1);
						box-shadow: 0px 2px 0px 1px rgba(39,98,158,1);
					}

					.pal-button-image{
						background: #45aada;
					}

					.pal-checkbox input:checked ~ .checkmark {
					    background-color: #3aafe6;
					}

					.pal-checkbox .checkmark:after{
						color: red
					}

					.carousel .thumb.selected, .carousel .thumb:hover{
						border-color: #3aafe6;
					}

					.pal-wrapper > div > svg{
						fill: #3aafe6;
					}

					.pal-cancel{
						color: #3aafe6;
					}

					.Dropdown-option.is-selected{
						background-color: #3aafe6;
					}
	    		`}</style>
	    	</Header>
  		)

  		if (this.state.render === 0)
  		return(
  			<div className={"pal-content"}>
  				{renderDynamicStyle()}
		    	<Welcome history={history} handleRenderApp={this.handleRenderApp.bind(this)}/>
	    	</div>
  		);

	    return(
	    	<div className={"pal-content"}>
	    		{renderDynamicStyle()}

		    	<CancelButton />

		    	<div className={"pal-wrapper align-center"}>
		    		<HelpButton />
		    		<img className={"pal-logo"} alt={"Logo"} src={logo}/>

			    	<Router basename={"/"} history={history}>
			    		<Route />
			    	</Router>
		    	</div>

		    	<div className={"pal-promotion"}>
		    		{this.state.promotion.images.length < 1 ? null : (
		    			<Carousel emulateTouch showThumbs={false} 
			    			showStatus={false} showArrows={false} infiniteLoop autoPlay interval={this.state.promotion.interval}>
			    			{this.state.promotion.images.map((data, index)=>(
			    				<div key={`Promo ${index}`}>
									<img alt={`Promo ${index}`} src={data} />
								</div>
			    			))}
						</Carousel>
		    		)}
		    	</div>
	    	</div>
	    );
	}
}

export default connect(store=>{
	return {
		background: store.app.background,
		logo: store.app.logo
	}
})(View);