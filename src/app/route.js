import React, { Component } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";

import Sample from "../modelview/empty"

import Auth from "../modelview/auth"
import Breakfast from "../modelview/breakfast"
import CardDetails from "../modelview/card-details"
import CardNew from "../modelview/card-new"
import CheckStatus from "../modelview/checkstatus"
import CheckinConfirmation from "../modelview/checkin-confirmation"
import CheckinSuccess from "../modelview/checkin-success"
import CheckoutBill from "../modelview/checkout-bill"
import CheckoutConfirmation from "../modelview/checkout-confirmation"
import CheckoutSuccess from "../modelview/checkout-success"
import CompanionDetails from "../modelview/companion-details"
import CompanionSignature from "../modelview/companion-signature"
import EarlyCheckin from "../modelview/early-checkin"
import FacialVerification from "../modelview/facial-verification"
import LateCheckout from "../modelview/late-checkout"
import MakeKey from "../modelview/make-key"
import PassportScan from "../modelview/passport-scan"
import PreCheckin from "../modelview/pre-checkin"
import PreCheckout from "../modelview/pre-checkout"
import ReservationDetail from "../modelview/reservation-details"
import RoomChoices from "../modelview/room-choices"
import RoomDescription from "../modelview/room-description"
import RoomPreferences from "../modelview/room-preferences"
import Welcome from "../modelview/welcome"
import GuestDetails from "../modelview/guest"
import PickupKey from "../modelview/pickup-key"

class View extends Component {
  	render(){
	    return(
	    	<Switch>
	    		<Route exact path={"/auth/:macAddress"} component={Auth}/>
	    		<Route exact path={"/sample"} component={Sample}/>

	    		<Route exact path={"/"} component={Welcome}/>
	    		<Route exact path={"/breakfast"} component={Breakfast}/>
	    		<Route exact path={"/card/details"} component={CardDetails}/>
	    		<Route exact path={"/card/new"} component={CardNew}/>
	    		<Route exact path={"/checkin/confirmation"} component={CheckinConfirmation}/>
	    		<Route exact path={"/checkin/success"} component={CheckinSuccess}/>
	    		<Route exact path={"/check-status"} component={CheckStatus}/>
	    		<Route exact path={"/checkout/bill"} component={CheckoutBill}/>
	    		<Route exact path={"/checkout/confirmation"} component={CheckoutConfirmation}/>
	    		<Route exact path={"/checkout/success"} component={CheckoutSuccess}/>
	    		<Route exact path={"/companion/details"} component={CompanionDetails}/>
	    		<Route exact path={"/companion/signature"} component={CompanionSignature}/>
	    		<Route exact path={"/early-checkin"} component={EarlyCheckin}/>
	    		<Route exact path={"/facial-verification"} component={FacialVerification}/>
	    		<Route exact path={"/late-checkout"} component={LateCheckout}/>
	    		<Route exact path={"/make-key"} component={MakeKey}/>
	    		<Route exact path={"/passport-scan"} component={PassportScan}/>
	    		<Route exact path={"/pre-checkin"} component={PreCheckin}/>
	    		<Route exact path={"/pre-checkout"} component={PreCheckout}/>
	    		<Route exact path={"/reservation-details"} component={ReservationDetail}/>
	    		<Route exact path={"/room/choices"} component={RoomChoices}/>
	    		<Route exact path={"/room/description"} component={RoomDescription}/>
	    		<Route exact path={"/room/preferences"} component={RoomPreferences}/>
	    		<Route exact path={"/guest/details"} component={GuestDetails}/>
	    		<Route exact path={"/pickup-key"} component={PickupKey}/>
				<Redirect from={"*"} to={"/"}/>
			</Switch>
	    );
	}
}

export default View;