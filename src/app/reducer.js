import {
	THEME_SET
} from "./constant";

const initialState = {
	background: {
		image: null,
		video: null
	},
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case THEME_SET:
			return setTheme(state, action);
		default:
			return state;
	}
}

function setTheme(state, action){
	return {
		...state,
		logo: action.logo,
		background: action.background
	}
}

export default reducer;