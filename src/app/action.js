import {
	THEME_SET
} from "./constant";

class Action{
	static setTheme(data){
		return {
			type: THEME_SET,
			logo: data.logo,
			background: data.background
		}
	}
}

export default Action;