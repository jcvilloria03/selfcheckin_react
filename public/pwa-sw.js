let deferredPrompt;

self.addEventListener('activate', function(event) {
	console.log('[Service Worker] Activating Service Worker ....', event);
});

self.addEventListener('appinstalled', (evt) => {
  console.log("installed");
});

self.addEventListener('beforeinstallprompt', (e) => {
  e.preventDefault();

  deferredPrompt = e;

  btnAdd.style.display = 'block';
  btnAdd.addEventListener('click', (e) => {
	  btnAdd.style.display = 'none';

	  deferredPrompt.prompt();
	  deferredPrompt.userChoice
	    .then((choiceResult) => {
	      if (choiceResult.outcome === 'accepted') {
	        console.log('User accepted the A2HS prompt');
	      } else {
	        console.log('User dismissed the A2HS prompt');
	      }
	      deferredPrompt = null;
	    });
	});
});

self.addEventListener('install', function(event) {
	
});

self.addEventListener('fetch', function(event) {
	
});